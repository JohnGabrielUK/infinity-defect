# Infinity Defect

_Infinity Defect_ was my entry to the tenth Godot Wild Jam.

# What can I do with this?

The game code is licensed under MIT, so you're free to use the code in your own work however you please. The assets, on the other hand, are copyrighted.
