extends Node

onready var files = {
	0: {
		"filename": "substance.mail",
		"size": 491,
		"contents": [
			"TO: All C4-R7 residents",
			"FROM: Mgmt",
			"SUBJECT: Substance handling procedures",
			"",
			"Dear all,",
			"",
			"We would like to remind you that the substances we are currently",
			"processing in the labs are highly volatile and should not be",
			"allowed to come into contact with bare skin. We have already had",
			"to admit three people to the medical bay with severe burns to",
			"their lower regions.",
			"",
			"Let's leave the experimenting to the scientists, please.",
			"",
			"Best,",
			"Mgmt"
		]
	},
	1: {
		"filename": "gravity.mail",
		"size": 629,
		"contents": [
			"TO: Protz, Werner",
			"FROM: Mgmt",
			"SUBJECT: RE: Gravity",
			"",
			"Dear Dr. Protz,",
			"",
			"While we appreciate the unusual circumstances you and your team",
			"are working under, it is vitally important that your research is",
			"completed as soon as possible. Therefore, the station will",
			"remain at its current orbiting distance from the anomaly for the",
			"duration of the mission.",
			"",
			"Please make do as best you can.",
			"",
			"Best,",
			"Mgmt",
			"",
			"P.S. Please stop using expense code 0054. That is for",
			"replacement parts in the engine room, not for cybernetic",
			"limbs, no matter whose fault the accidents are."
		]
	},
	2: {
		"filename": "computer.mail",
		"size": 378,
		"contents": [
			"TO: All C4-R7 residents",
			"FROM: Mgmt",
			"SUBJECT: Computer",
			"",
			"Dear all,",
			"",
			"Contrary to what you may have heard, the station's computer is",
			"not programmed to respond to quotes from old sci-fi movies. If",
			"you ask it to open the pod bay doors when standing next to an",
			"airlock, you have only yourself to blame for what happens next.",
			"",
			"Best,",
			"Mgmt"
		]
	},
	3: {
		"filename": "reanimation.mail",
		"size": 469,
		"contents": [
			"TO: All C4-R7 residents",
			"FROM: Mgmt",
			"SUBJECT: Reanimation chamber",
			"",
			"Dear all,",
			"",
			"We have noticed that the logs from the reanimation chamber show",
			"far more usages than reported accidents. May we remind you that",
			"this is an experimental device, procured at great expense, and",
			"is intended only to mitigate the hazardous conditions of your",
			"current working environment.",
			"",
			"Further abuse will not be tolerated.",
			"",
			"Best,",
			"Mgmt"
		]
	}
}

onready var computers = {
	0: {
		"files": [0, 1, 2, 3]
	}
}

func get_computer_files(computer_id : int) -> Array:
	return computers[computer_id]["files"]

func get_file_id_by_name(filename : String) -> int:
	for current_file in files:
		if files[current_file]["filename"] == filename:
			return current_file
	return -1

func get_file_name(file_id : int) -> String:
	return files[file_id]["filename"]

func get_file_size(file_id : int) -> int:
	return files[file_id]["size"]

func get_file_contents(file_id : int) -> Array:
	return files[file_id]["contents"]