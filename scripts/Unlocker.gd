extends Node2D

onready var harddoor_engine = $HardDoor_Engine
onready var harddoor_science = $HardDoor_Science

func _ready():
	var opened = 0
	if GameProgress.engine_clear_state == GameProgress.ROOM_STATE.CLEARED:
		harddoor_engine.open()
		opened += 1
	if GameProgress.science_clear_state == GameProgress.ROOM_STATE.CLEARED:
		harddoor_science.open()
		opened += 1
	if opened != 2:
			get_tree().call_group("hud", "show_prompt", "Master keys required", 0.2)
