extends Control

const CREDITS : Array = [
	["Design & code", "John Gabriel (@JohnGabrielUK)"],
	["Pixel art", "Mateus Medeiros (@mateuscarmed)"],
	["Sound design", "Vidar Leimar (@LeimSound)"],
	["Music", "Kevin MacLeod (Incompetech)"]
]

onready var label_energy_value = $Label_Energy_Value
onready var label_time_label = $Label_Time_Label
onready var label_time_value = $Label_Time_Value
onready var label_anomaly_label = $Label_Anomaly_Label
onready var label_anomaly_value = $Label_Anomaly_Value
onready var label_prompt = $Label_Prompt
onready var label_credits_role = $Label_Credits_Role
onready var label_credits_name = $Label_Credits_Name
onready var tween = $Tween
onready var tween_credits = $Tween_Credits

var energy_displayed : int = 0
var energy_actual : int = 0

func show_prompt(text : String, type_time : float) -> void:
	label_prompt.text = text
	label_prompt.percent_visible = 0.0
	label_prompt.modulate = Color.white
	label_prompt.show()
	tween.interpolate_property(label_prompt, "percent_visible", 0.0, 1.0, type_time, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()
	yield(get_tree().create_timer(5.0), "timeout")
	tween.interpolate_property(label_prompt, "modulate", Color.white, Color(1.0, 1.0, 1.0, 0.0), 1.0, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()

func show_credits() -> void:
	yield(get_tree().create_timer(2.0), "timeout")
	for current_credit in CREDITS:
		label_credits_role.text = current_credit[0]
		label_credits_role.percent_visible = 0.0
		label_credits_role.modulate = Color.white
		label_credits_role.show()
		label_credits_name.text = current_credit[1]
		label_credits_name.percent_visible = 0.0
		label_credits_name.modulate = Color.white
		label_credits_name.show()
		tween_credits.interpolate_property(label_credits_role, "percent_visible", 0.0, 1.0, 1.0, Tween.TRANS_LINEAR, Tween.EASE_IN)
		tween_credits.start()
		yield(tween_credits, "tween_all_completed")
		tween_credits.interpolate_property(label_credits_name, "percent_visible", 0.0, 1.0, 1.0, Tween.TRANS_LINEAR, Tween.EASE_IN)
		tween_credits.start()
		yield(tween_credits, "tween_all_completed")
		yield(get_tree().create_timer(2.0), "timeout")
		tween_credits.interpolate_property(label_credits_role, "modulate", Color.white, Color(1.0, 1.0, 1.0, 0.0), 1.0, Tween.TRANS_LINEAR, Tween.EASE_IN)
		tween_credits.interpolate_property(label_credits_name, "modulate", Color.white, Color(1.0, 1.0, 1.0, 0.0), 1.0, Tween.TRANS_LINEAR, Tween.EASE_IN)
		tween_credits.start()
		yield(tween_credits, "tween_all_completed")

func set_player_energy(energy : int) -> void:
	energy_actual = energy

func set_time_remaining(time_remaining : float) -> void:
	var msec = fmod(time_remaining, 1.0) * 1000
	var sec = fmod(time_remaining, 60.0)
	var minute = floor(time_remaining / 60.0)
	var string = "%02d:%02d.%03d" % [minute, sec, msec]
	label_time_value.text = string
	label_time_value.update()

func set_anomaly_time(time_remaining : float) -> void:
	var msec = fmod(time_remaining, 1.0) * 1000
	var sec = fmod(time_remaining, 60.0)
	var string = "%02d.%03d" % [sec, msec]
	label_anomaly_value.text = string
	label_anomaly_value.update()

func set_hud_visible(value : bool) -> void:
	visible = value

func set_time_visible(value : bool) -> void:
	label_time_label.visible = value
	label_time_value.visible = value

func set_anomaly_visible(value : bool) -> void:
	label_anomaly_label.visible = value
	label_anomaly_value.visible = value

func _on_Timer_Update_timeout() -> void:
	if energy_displayed > energy_actual:
		energy_displayed -= 1
	elif energy_displayed < energy_actual:
		energy_displayed += 1
	label_energy_value.text = str(energy_displayed)
