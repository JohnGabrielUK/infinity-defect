extends Node2D

onready var timer : Timer = $Timer
onready var audio_change = $Audio_Change

func _physics_process(delta : float) -> void:
	if timer.time_left <= 5.0:
		get_tree().call_group("hud", "set_anomaly_visible", true)
		get_tree().call_group("hud", "set_anomaly_time", timer.time_left)

func _on_Timer_timeout() -> void:
	get_tree().call_group("player", "skew_controls")
	get_tree().call_group("hud", "set_anomaly_visible", false)
	audio_change.play()
