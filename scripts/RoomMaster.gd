extends Node

onready var tween = $Tween
onready var filter = $Overlay/Filter
onready var blackout = $Overlay/Blackout
onready var hud = $Overlay/HUD

onready var rooms : Dictionary = {
	"hangar": {
		"scene": "res://scenes/rooms/Hangar.tscn",
		"music": AudioMaster.MUSIC_TRACK.NONE,
		"ambience": AudioMaster.AMBIENCE_TRACK.HANGAR,
		"glitch_track_levels": [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
		"visual_glitches": {"enabled": false}
	},
	"corridor_a": {
		"scene": "res://scenes/rooms/CorridorA.tscn",
		"music": AudioMaster.MUSIC_TRACK.PHANTOM_FROM_SPACE,
		"ambience": AudioMaster.AMBIENCE_TRACK.CORRIDOR,
		"glitch_track_levels": [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
		"visual_glitches": {"enabled": false}
	},
	"hub_a": {
		"scene": "res://scenes/rooms/HubA.tscn",
		"music": AudioMaster.MUSIC_TRACK.PHANTOM_FROM_SPACE,
		"ambience": AudioMaster.AMBIENCE_TRACK.HUB,
		"glitch_track_levels": [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
		"visual_glitches": {"enabled": false}
	},
	"save_room_a": {
		"scene": "res://scenes/rooms/SaveRoomA.tscn",
		"music": AudioMaster.MUSIC_TRACK.PHANTOM_FROM_SPACE,
		"ambience": AudioMaster.AMBIENCE_TRACK.HUB,
		"glitch_track_levels": [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
		"visual_glitches": {"enabled": true, "limit": 0.45, "loops": 10, "start": 0, "time_multiplier": 20.0,
						"step_amount": 0.1, "power": 1.5, "resolution": 8.0}
	},
	"corridor_b": {
		"scene": "res://scenes/rooms/CorridorB.tscn",
		"music": AudioMaster.MUSIC_TRACK.NONE,
		"ambience": AudioMaster.AMBIENCE_TRACK.CORRIDOR,
		"glitch_track_levels": [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
		"visual_glitches": {"enabled": false}
	},
	"engine_room_a": {
		"scene": "res://scenes/rooms/EngineRoomA.tscn",
		"music": AudioMaster.MUSIC_TRACK.FUTURE_GLADIATOR,
		"ambience": AudioMaster.AMBIENCE_TRACK.ENGINEERING,
		"glitch_track_levels": [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
		"visual_glitches": {"enabled": false}
	},
	"corridor_c": {
		"scene": "res://scenes/rooms/CorridorC.tscn",
		"music": AudioMaster.MUSIC_TRACK.NONE,
		"ambience": AudioMaster.AMBIENCE_TRACK.CORRIDOR,
		"glitch_track_levels": [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
		"visual_glitches": {"enabled": false}
	},
	"science_lab_a": {
		"scene": "res://scenes/rooms/ScienceLabA.tscn",
		"music": AudioMaster.MUSIC_TRACK.RISING_TIDE,
		"ambience": AudioMaster.AMBIENCE_TRACK.SCIENCE,
		"glitch_track_levels": [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
		"visual_glitches": {"enabled": false}
	},
	"obstacle": {
		"scene": "res://scenes/rooms/Obstacle.tscn",
		"music": AudioMaster.MUSIC_TRACK.NONE,
		"ambience": AudioMaster.AMBIENCE_TRACK.CORRIDOR,
		"glitch_track_levels": [0.0, 0.0, 0.0, 0.0, 0.0, 0.35],
		"visual_glitches": {"enabled": true, "limit": 0.885, "loops": 15, "start": 0, "time_multiplier": 30.0,
						"step_amount": 0.1, "power": 1.25, "resolution": 8.0}
	},
	"maze": {
		"scene": "res://scenes/rooms/Maze.tscn",
		"music": AudioMaster.MUSIC_TRACK.NONE,
		"ambience": AudioMaster.AMBIENCE_TRACK.CORRIDOR,
		"glitch_track_levels": [0.0, 0.25, 0.0, 0.3, 0.25, 0.45],
		"visual_glitches": {"enabled": true, "limit": 0.5, "loops": 6, "start": -3, "time_multiplier": 100.0,
						"step_amount": 0.155, "power": 2.5, "resolution": 3.0}
	},
	"corridor_d": {
		"scene": "res://scenes/rooms/CorridorD.tscn",
		"music": AudioMaster.MUSIC_TRACK.UNSEEN_HORRORS,
		"ambience": AudioMaster.AMBIENCE_TRACK.FINALE,
		"glitch_track_levels": [0.5, 0.5, 0.5, 0.5, 0.5, 0.5],
		"visual_glitches": {"enabled": true, "limit": 0.3, "loops": 15, "start": 0, "time_multiplier": 15.0,
						"step_amount": 0.1, "power": 1.5, "resolution": 8.0}
	},
	"corridor_e": {
		"scene": "res://scenes/rooms/CorridorE.tscn",
		"music": AudioMaster.MUSIC_TRACK.UNSEEN_HORRORS,
		"ambience": AudioMaster.AMBIENCE_TRACK.FINALE,
		"glitch_track_levels": [0.5, 0.5, 0.5, 0.5, 0.5, 0.5],
		"visual_glitches": {"enabled": true, "limit": 0.3, "loops": 15, "start": 0, "time_multiplier": 15.0,
						"step_amount": 0.1, "power": 1.5, "resolution": 8.0}
	},
	"boss_chamber": {
		"scene": "res://scenes/rooms/BossChamber.tscn",
		"music": AudioMaster.MUSIC_TRACK.NONE,
		"ambience": AudioMaster.AMBIENCE_TRACK.FINALE,
		"glitch_track_levels": [0.5, 0.5, 0.5, 0.5, 0.5, 0.5],
		"visual_glitches": {"enabled": true, "limit": 0.3, "loops": 15, "start": 0, "time_multiplier": 15.0,
						"step_amount": 0.1, "power": 1.5, "resolution": 8.0}
	}
}

var current_room : Node

onready var self_destruct_activated : bool = false
onready var self_destruct_time_remaining : float = 150.0

func set_shader_parameters(enabled : bool, tear_limit : float, loops : int, start_power : int,
							time_multiplier : float, step_amount : float, power : float, resolution: float):
	filter.get_material().set_shader_param("glitches_enabled", enabled)
	filter.get_material().set_shader_param("tear_limit", tear_limit)
	filter.get_material().set_shader_param("loops", loops)
	filter.get_material().set_shader_param("start_power", start_power)
	filter.get_material().set_shader_param("time_multiplier", time_multiplier)
	filter.get_material().set_shader_param("step_amount", step_amount)
	filter.get_material().set_shader_param("power", power)
	filter.get_material().set_shader_param("resolution", resolution)

func check_for_voiceover(destination_room : String, entrance_id : int) -> void:
	# Most audios aren't for on reloading games, but one in particular is
	if entrance_id == 99:
		if GameProgress.times_reloaded_since_last_save >= 3:
			if not GameProgress.voiceovers_played["vo7"]:
				AudioMaster.play_voiceover("vo7")
		return
	match destination_room:
		"hangar":
			if not GameProgress.voiceovers_played["vo1"]:
				AudioMaster.play_voiceover("vo1")
		"corridor_a":
			if not GameProgress.voiceovers_played["vo2"]:
				AudioMaster.play_voiceover("vo2")
		"corridor_b":
			if GameProgress.engine_clear_state == GameProgress.ROOM_STATE.UNSEEN:
				if GameProgress.voiceovers_played["vo3_lab"]:
					if not GameProgress.voiceovers_played["vo4_engine"]:
						AudioMaster.play_voiceover("vo4_engine")
				else:
					if not GameProgress.voiceovers_played["vo3_engine"]:
						AudioMaster.play_voiceover("vo3_engine")
			elif GameProgress.engine_clear_state == GameProgress.ROOM_STATE.ENTERED:
				if not GameProgress.voiceovers_played["vo5"]:
					AudioMaster.play_voiceover("vo5")
					GameProgress.voiceovers_played["vo5"] = true
				elif not GameProgress.voiceovers_played["vo6"]:
					AudioMaster.play_voiceover("vo6")
		"engine_room_a":
			if GameProgress.engine_clear_state == GameProgress.ROOM_STATE.UNSEEN:
				 GameProgress.engine_clear_state = GameProgress.ROOM_STATE.ENTERED
		"corridor_c":
			if GameProgress.science_clear_state == GameProgress.ROOM_STATE.UNSEEN:
				if GameProgress.voiceovers_played["vo3_engine"]:
					if not GameProgress.voiceovers_played["vo4_lab"]:
						AudioMaster.play_voiceover("vo4_lab")
				else:
					if not GameProgress.voiceovers_played["vo3_lab"]:
						AudioMaster.play_voiceover("vo3_lab")
						GameProgress.voiceovers_played["vo3_lab"] = true
			elif GameProgress.science_clear_state == GameProgress.ROOM_STATE.ENTERED:
				if not GameProgress.voiceovers_played["vo5"]:
					AudioMaster.play_voiceover("vo5")
					GameProgress.voiceovers_played["vo5"] = true
				elif not GameProgress.voiceovers_played["vo6"]:
					AudioMaster.play_voiceover("vo6")
					GameProgress.voiceovers_played["vo6"] = true
		"science_lab_a":
			if GameProgress.science_clear_state == GameProgress.ROOM_STATE.UNSEEN:
				 GameProgress.science_clear_state = GameProgress.ROOM_STATE.ENTERED
		"hub_a":
			if GameProgress.science_clear_state == GameProgress.ROOM_STATE.CLEARED and GameProgress.engine_clear_state == GameProgress.ROOM_STATE.CLEARED:
				if not GameProgress.voiceovers_played["vo9"]:
					AudioMaster.play_voiceover("vo9")
					GameProgress.voiceovers_played["vo9"] = true
			

func init_room(destination_room : String, entrance_id: int) -> void:
	GameProgress.current_room = destination_room
	GameProgress.current_entrance = entrance_id
	AudioMaster.play_track(rooms[destination_room]["ambience"])
	var scene_path = rooms[destination_room]["scene"]
	# Change the music/glitches, unless we're in the self-destruct sequence
	if not (self_destruct_activated or GameProgress.final_boss_destroyed):
		AudioMaster.set_glitch_track_levels(rooms[destination_room]["glitch_track_levels"])
		AudioMaster.play_music(rooms[destination_room]["music"])
	# Wait for the music to be finished loading before we proceed
	current_room = load(scene_path).instance()
	call_deferred("add_child", current_room)
	current_room.connect("room_exited", self, "room_exited")
	current_room.call_deferred("spawn_player", entrance_id)
	# Update shaders
	var glitchiness = rooms[destination_room]["visual_glitches"]
	if glitchiness["enabled"] == false or self_destruct_activated:
		set_shader_parameters(false, 0, 0, 0, 0, 0, 0, 0)
	else:
		set_shader_parameters(true, glitchiness["limit"], glitchiness["loops"], glitchiness["start"],
								glitchiness["time_multiplier"], glitchiness["step_amount"],
								glitchiness["power"], glitchiness["resolution"])
	# Check for voiceovers
	check_for_voiceover(destination_room, entrance_id)

func room_exited(destination_room : String, destination_exit_id : int) -> void:
	hud.set_anomaly_visible(false) # janky hack, m8!
	get_tree().paused = true
	tween.interpolate_property(blackout, "modulate", Color(1.0, 1.0, 1.0, 0.0), Color.white, 0.25, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.start()
	yield(tween, "tween_all_completed")
	yield(get_tree().create_timer(0.5), "timeout")
	current_room.queue_free()
	get_tree().paused = false
	init_room(destination_room, destination_exit_id)
	tween.interpolate_property(blackout, "modulate", Color.white, Color(1.0, 1.0, 1.0, 0.0), 0.25, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.start()

func gameover() -> void:
	AudioMaster.play_music(AudioMaster.MUSIC_TRACK.NONE)
	AudioMaster.play_gameover()
	yield(get_tree().create_timer(0.2), "timeout")
	get_tree().paused = true
	# Set up the glitchy transition
	var glitchiness = 0.0
	while glitchiness < 2.0:
		glitchiness += 0.1
		set_shader_parameters(true, 1, 5, 0, 30, glitchiness, 1.5, 8)
		yield(get_tree().create_timer(0.15), "timeout")
	AudioMaster.stop_all()
	get_tree().change_scene("res://scenes/GameOver.tscn")

func begin_self_destruct() -> void:
	GameProgress.final_boss_destroyed = true
	get_tree().call_group("player", "add_health", 1000)
	get_tree().call_group("player", "set_camera_self_destruct_effect")
	# A moment's silence
	AudioMaster.set_glitch_track_levels([0, 0, 0, 0, 0, 0])
	AudioMaster.stop_all()
	set_shader_parameters(false, 0, 0, 0, 0, 0, 0, 0)
	yield(get_tree().create_timer(1.0), "timeout")
	AudioMaster.play_self_destruct_tracks()
	yield(get_tree().create_timer(6.0), "timeout")
	# Aw shit, here we go again
	AudioMaster.play_music(AudioMaster.MUSIC_TRACK.AGGRESSOR)
	hud.show_prompt("Reactor in meltdown\nEvacuate immediately", 2.0)
	yield(get_tree().create_timer(2.0), "timeout")
	self_destruct_activated = true
	hud.set_time_visible(true)

func goto_ending() -> void:
	AudioMaster.stop_all()
	tween.interpolate_property(blackout, "modulate", Color(1.0, 1.0, 1.0, 0.0), Color.white, 0.50, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.start()
	yield(tween, "tween_all_completed")
	get_tree().change_scene("res://scenes/Ending.tscn")

func _physics_process(delta) -> void:
	if self_destruct_activated:
		self_destruct_time_remaining -= delta
		hud.set_time_remaining(self_destruct_time_remaining)
		if self_destruct_time_remaining < 0.0:
			gameover()
			self_destruct_activated = false

func _ready() -> void:
	AudioMaster.init_glitch_layers()
	yield(get_tree().create_timer(0.5), "timeout")
	init_room(GameProgress.current_room, GameProgress.current_entrance)
	tween.interpolate_property(blackout, "modulate", Color.white, Color(1.0, 1.0, 1.0, 0.0), 1.00, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.start()
	yield(tween, "tween_all_completed")
	get_tree().call_group("hud", "set_hud_visible", true)
	if GameProgress.current_room == "hangar":
		get_tree().call_group("hud", "show_credits")
