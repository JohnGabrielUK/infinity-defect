extends Control
tool

const FONT = preload("res://fonts/hud.tres")

var text : String = "00:00.000"

func _draw() -> void:
	var offset : Vector2 = Vector2.ZERO
	for current_char in text:
		var this_offset = Vector2.ZERO
		if current_char == "1": this_offset = Vector2(8.0, 0.0)
		draw_string(FONT, rect_position + this_offset + offset, current_char)
		if current_char in [":", "."]: offset.x += 6.0
		else: offset.x += 22.0
