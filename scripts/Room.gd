extends Node2D

const OBJ_PLAYER = preload("res://objects/world_objects/Player.tscn")

onready var camera_limit : Position2D = $CameraLimit

signal room_exited

func spawn_player(entrance_id : int) -> void:
	var spawn_position : Vector2 = Vector2.ZERO
	# Find the entrance to spawn them at
	for current_entrance in get_tree().get_nodes_in_group("room_entrance"):
		if current_entrance.entrance_id == entrance_id and is_a_parent_of(current_entrance):
			spawn_position = current_entrance.global_position
			current_entrance.used()
			break
	# Make sure we found somewhere to spawn!
	if spawn_position == Vector2.ZERO:
		print("ERROR: spawn location not found! Defaulting to [0,0].")
	# Spawn the player
	var player = OBJ_PLAYER.instance()
	player.global_position = spawn_position
	add_child(player)
	# Spawn camera, and set limits
	player.set_room_limits(Rect2(Vector2.ZERO, camera_limit.global_position))

func exit_entered(destination_room : String, destination_entrance_id : int) -> void:
	get_tree().call_group("control_skewer", "queue_free") # janky hack, m8!
	get_tree().call_group("player", "update_game_progress")
	emit_signal("room_exited", destination_room, destination_entrance_id)

func _ready():
	for current_exit in get_tree().get_nodes_in_group("room_exit"):
		current_exit.connect("room_exited", self, "exit_entered")
