extends Node

const NUMBER_OF_E_TANKS : int = 5

enum ROOM_STATE {UNSEEN, ENTERED, CLEARED}

var player_health : int
var player_max_health : int
var current_room : String
var current_entrance : int
var science_clear_state : int
var engine_clear_state : int
var etanks_picked_up : Array
var voiceovers_played : Dictionary
var final_boss_destroyed : bool

onready var times_reloaded_since_last_save : int = 0

var saved_game : Dictionary

func get_current_state() -> Dictionary:
	return {
		"health": int(player_health), "max_health": int(player_max_health),
		"room": String(current_room), "entrance": int(current_entrance),
		"science_progress": int(science_clear_state),
		"engine_progress": int(engine_clear_state),
		"etanks": etanks_picked_up.duplicate(true), "vo": voiceovers_played.duplicate(true)
	}

func save_game() -> void:
	saved_game = get_current_state()
	times_reloaded_since_last_save = 0

func restore_saved_game() -> void:
	player_health = saved_game["health"]
	player_max_health = saved_game["max_health"]
	current_room = saved_game["room"]
	current_entrance = saved_game["entrance"]
	science_clear_state = saved_game["science_progress"]
	engine_clear_state = saved_game["engine_progress"]
	etanks_picked_up = saved_game["etanks"]
	voiceovers_played = saved_game["vo"]
	times_reloaded_since_last_save += 1
	final_boss_destroyed = false

func etank_picked_up(which : int) -> void:
	if etanks_picked_up[which] == false:
		etanks_picked_up[which] = true
		player_max_health += 30
		# Update the player's health
		var player_nodes = get_tree().get_nodes_in_group("player")
		if player_nodes.size() > 0:
			var player = player_nodes[0]
			player.max_health += 30
		get_tree().call_group("hud", "show_prompt", "Energy tank acquired", 0.1)

func key_picked_up(type : int) -> void:
	match type:
		0:
			science_clear_state = ROOM_STATE.CLEARED
			get_tree().call_group("hud", "show_prompt", "Master key A acquired", 0.1)
		1:
			engine_clear_state = ROOM_STATE.CLEARED
			get_tree().call_group("hud", "show_prompt", "Master key B acquired", 0.1)

func new_game() -> void:
	player_health = 50
	player_max_health = 50
	current_room = "hangar"
	current_entrance = 0
	science_clear_state = ROOM_STATE.UNSEEN
	engine_clear_state = ROOM_STATE.UNSEEN
	voiceovers_played = {}
	final_boss_destroyed = false
	for current_voiceover in AudioMaster.VOICEOVER_TRACKS:
		voiceovers_played[current_voiceover] = false
	etanks_picked_up = []
	for i in range(0, NUMBER_OF_E_TANKS):
		etanks_picked_up.append(false)
	save_game()

func _ready():
	new_game()
