extends Node2D

enum MUSIC_TRACK {NONE, IT_IS_LOST, OUTFOXING_THE_FOX, PHANTOM_FROM_SPACE, FUTURE_GLADIATOR, RISING_TIDE, UNSEEN_HORRORS, AGGRESSOR}
enum AMBIENCE_TRACK {CORRIDOR, ENGINEERING, FINALE, HANGAR, HUB, SCIENCE}

const AMBIENCE_TRACKS : Dictionary = {
	AMBIENCE_TRACK.CORRIDOR: preload("res://sounds/ambience/corridor.ogg"),
	AMBIENCE_TRACK.ENGINEERING: preload("res://sounds/ambience/engineering.ogg"),
	AMBIENCE_TRACK.FINALE: preload("res://sounds/ambience/finale.ogg"),
	AMBIENCE_TRACK.HANGAR: preload("res://sounds/ambience/hangar.ogg"),
	AMBIENCE_TRACK.HUB: preload("res://sounds/ambience/hub.ogg"),
	AMBIENCE_TRACK.SCIENCE: preload("res://sounds/ambience/science.ogg")
}

const GLITCH_TRACKS : Array = [
	preload("res://sounds/ambience/glitch/glitch1.ogg"),
	preload("res://sounds/ambience/glitch/glitch2.ogg"),
	preload("res://sounds/ambience/glitch/glitch3.ogg"),
	preload("res://sounds/ambience/glitch/glitch4.ogg"),
	preload("res://sounds/ambience/glitch/glitch5.ogg"),
	preload("res://sounds/ambience/glitch/glitch6.ogg")
]

const VOICEOVER_TRACKS : Dictionary = {
	"vo1": preload("res://sounds/voiceover/vo1.ogg"),
	"vo2": preload("res://sounds/voiceover/vo2.ogg"),
	"vo3_lab": preload("res://sounds/voiceover/vo3_lab.ogg"),
	"vo3_engine": preload("res://sounds/voiceover/vo3_engine.ogg"),
	"vo4_lab": preload("res://sounds/voiceover/vo4_lab.ogg"),
	"vo4_engine": preload("res://sounds/voiceover/vo4_engine.ogg"),
	"vo5": preload("res://sounds/voiceover/vo5.ogg"),
	"vo6": preload("res://sounds/voiceover/vo6.ogg"),
	"vo7": preload("res://sounds/voiceover/vo7.ogg"),
	"vo8": preload("res://sounds/voiceover/vo8.ogg"),
	"vo9": preload("res://sounds/voiceover/vo9.ogg"),
	"vo10": preload("res://sounds/voiceover/vo10.ogg"),
}

const MUSIC_TRACKS : Dictionary = {
	MUSIC_TRACK.NONE: null,
	MUSIC_TRACK.IT_IS_LOST: preload("res://music/it-is-lost-by-kevin-macleod.ogg"),
	MUSIC_TRACK.PHANTOM_FROM_SPACE: preload("res://music/phantom-from-space-by-kevin-macleod.ogg"),
	MUSIC_TRACK.FUTURE_GLADIATOR: preload("res://music/future-gladiator-by-kevin-macleod.ogg"),
	MUSIC_TRACK.RISING_TIDE: preload("res://music/rising-tide-faster-by-kevin-macleod.ogg"),
	MUSIC_TRACK.UNSEEN_HORRORS: preload("res://music/unseen-horrors-by-kevin-macleod.ogg"),
	MUSIC_TRACK.AGGRESSOR: preload("res://music/aggressor-by-kevin-macleod.ogg")
}

const MUSIC_TRACK_VOLUMES : Dictionary = {
	MUSIC_TRACK.NONE: 0.0,
	MUSIC_TRACK.IT_IS_LOST: 0.0,
	MUSIC_TRACK.PHANTOM_FROM_SPACE: 0.0,
	MUSIC_TRACK.FUTURE_GLADIATOR: -10.0,
	MUSIC_TRACK.RISING_TIDE: 0.0,
	MUSIC_TRACK.UNSEEN_HORRORS: 0.0,
	MUSIC_TRACK.AGGRESSOR: 0.0,
}

var glitch_players : Array

onready var player_a : AudioStreamPlayer = $AudioStreamPlayerA
onready var player_b : AudioStreamPlayer = $AudioStreamPlayerB
onready var player_music : AudioStreamPlayer = $AudioStreamPlayer_Music
onready var player_voiceover : AudioStreamPlayer = $AudioStreamPlayer_Voiceover
onready var player_gameover : AudioStreamPlayer = $AudioStreamPlayer_GameOver
onready var player_selfdestruct : AudioStreamPlayer = $AudioStreamPlayer_SelfDestruct
onready var player_selfdestruct_alarms : AudioStreamPlayer = $AudioStreamPlayer_SelfDestruct_Alarms
onready var player_selfdestruct_alarms_far : AudioStreamPlayer = $AudioStreamPlayer_SelfDestruct_Alarms_Distant
onready var anim_player = $AnimationPlayer_SelfDestruct

onready var tween : Tween = $Tween

onready var current_ambience_player : int = 0
onready var current_ambience : int = -1
onready var playing_ambience : bool = false
onready var current_music : int = -1

signal music_started

func play_gameover() -> void:
	player_gameover.play()

func stop_all() -> void:
	player_a.stop()
	player_b.stop()
	player_music.stop()
	player_selfdestruct.stop()
	player_selfdestruct_alarms.stop()
	player_selfdestruct_alarms_far.stop()
	for current_player in glitch_players: current_player.stop()
	current_ambience = -1
	playing_ambience = false

func play_track(which : int) -> void:
	# Make sure we aren't already playing_ambience the given track
	if current_ambience == which: return
	current_ambience = which
	if playing_ambience:
		var new_player : AudioStreamPlayer = player_b
		var old_player : AudioStreamPlayer = player_a
		if current_ambience_player == 1:
			new_player = player_a
			old_player = player_b
			current_ambience_player = 0
		else:
			current_ambience_player = 1
		new_player.volume_db = -30
		new_player.stream = AMBIENCE_TRACKS[which]
		new_player.call_deferred("play")
		tween.interpolate_property(new_player, "volume_db", new_player.volume_db, 0, 1.0, Tween.TRANS_LINEAR, Tween.EASE_IN)
		tween.interpolate_property(old_player, "volume_db", old_player.volume_db, -30, 1.0, Tween.TRANS_LINEAR, Tween.EASE_IN)
		tween.start()
		yield(tween, "tween_all_completed")
		tween.remove_all()
		old_player.stop()
	else:
		player_a.stream = AMBIENCE_TRACKS[which]
		player_a.call_deferred("play")
		playing_ambience = true

func set_glitch_track_levels(levels : Array) -> void:
	for i in range(0, 6):
		var current_level = levels[i]
		glitch_players[i].volume_db = linear2db(current_level)

func init_glitch_layers() -> void:
	glitch_players = []
	for current_track in GLITCH_TRACKS:
		var audio_player = AudioStreamPlayer.new()
		audio_player.stream = current_track
		add_child(audio_player)
		glitch_players.append(audio_player)
		audio_player.volume_db = linear2db(0.0)
		audio_player.call_deferred("play")

func play_self_destruct_tracks() -> void:
	anim_player.play("self_destruct")

func play_voiceover(which : String) -> void:
	player_voiceover.stop()
	player_voiceover.stream = VOICEOVER_TRACKS[which]
	player_voiceover.call_deferred("play")
	GameProgress.voiceovers_played[which] = true

func play_music(which : int) -> void: # Returns whether we need to wait
	if current_music == which:
		return
	if current_music != -1:
		tween.interpolate_property(player_music, "volume_db", player_music.volume_db, -30, 1.0, Tween.TRANS_LINEAR, Tween.EASE_IN)
		tween.start()
		yield(tween, "tween_all_completed")
		player_music.stop()
	current_music = which
	if which != MUSIC_TRACK.NONE:
		player_music.volume_db = MUSIC_TRACK_VOLUMES[which]
		player_music.stream = MUSIC_TRACKS[which]
		player_music.call_deferred("play")
		emit_signal("music_started")
