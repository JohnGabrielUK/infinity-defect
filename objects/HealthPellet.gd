extends Area2D

const ANIM_SPEED : float = 15.0

onready var sprite = $Sprite

var lifetime : float = 10.0

onready var anim_index : float = 0.0

func _physics_process(delta : float) -> void:
	lifetime -= delta
	if lifetime <= 0: queue_free()
	anim_index += delta * ANIM_SPEED
	sprite.frame = int(anim_index) % sprite.hframes

func _on_HealthPellet_body_entered(body):
	if body is Player:
		body.add_health(10, true)
		queue_free()
