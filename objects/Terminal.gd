extends CanvasLayer

const ALPHANUMERICS : String = "01234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ."
const HELP_LINES : Array = [
	"ls         - lists files in directory",
	"cat [file] - prints contents of file",
	"help       - lists known commands",
	"shutdown   - safely closes terminal"
]

const SOUND_RUNNING = preload("res://sounds/computer/running.ogg")
const SOUND_SHUTDOWN = preload("res://sounds/computer/shutdown.ogg")

onready var label = $Label
onready var audio = $AudioStreamPlayer
onready var anim_player = $AnimationPlayer

var output : String  = ""

var prompt : String = "$>"
var current_input : String = ""
var cursor : String = "_"

var computer_id = 0 # Which terminal in the game are we controlling?

onready var show_cursor = true
export (bool) var accept_input = false
onready var lines_displayed = 0

func list_files() -> void:
	accept_input = false
	var file_ids = ComputerMaster.get_computer_files(computer_id)
	for current_id in file_ids:
		var filename = ComputerMaster.get_file_name(current_id)
		while filename.length() < 16: filename += " "
		var filesize = ComputerMaster.get_file_size(current_id)
		add_line_to_display("%10s %6d bytes" % [filename, filesize])
		yield(get_tree().create_timer(0.1), "timeout")
	add_line_to_display("%d file(s) in directory" % file_ids.size())
	accept_input = true

func print_file(filename : String) -> void:
	accept_input = false
	var file_id : int = ComputerMaster.get_file_id_by_name(filename)
	if file_id == -1:
		add_line_to_display("file not found")
		accept_input = true
		return
	var contents : Array = ComputerMaster.get_file_contents(file_id)
	for current_line in contents:
		add_line_to_display(current_line)
		yield(get_tree().create_timer(0.04), "timeout")
	accept_input = true

func print_help() -> void:
	accept_input = false
	for current_line in HELP_LINES:
		add_line_to_display(current_line)
		yield(get_tree().create_timer(0.04), "timeout")
	accept_input = true

func exit():
	accept_input = false
	audio.stream = SOUND_SHUTDOWN
	audio.play()
	anim_player.play("shutdown")

func add_line_to_display(line : String) -> void:
	if output == "": output = line
	else: output += "\n" + line
	lines_displayed += 1
	if lines_displayed > 21:
		label.lines_skipped += 1
	update_display()

func clear_display() -> void:
	output = ""
	update_display()

func update_display() -> void:
	if not accept_input:
		label.text = output
		return
	label.text = output + "\n" + prompt + current_input
	if show_cursor: label.text += cursor

func process_input() -> void:
	add_line_to_display(prompt + current_input)
	var tokens = current_input.split(" ")
	tokens.resize(3)
	current_input = ""
	match tokens[0]:
		"ls": list_files()
		"cat": print_file(tokens[1])
		"help": print_help()
		"shutdown": exit()
		"cd": add_line_to_display("error: only local memory available; check network")
		"ping": add_line_to_display("pong!")
		"butts": add_line_to_display("butts.")
		_: add_line_to_display("bad command or file not found")

func _unhandled_input(event) -> void:
	if not accept_input: return
	if event is InputEventKey and event.pressed:
		get_tree().set_input_as_handled()
		# Check if it's a character key
		var scanstr = OS.get_scancode_string(event.scancode)
		if scanstr in ALPHANUMERICS:
			current_input += scanstr.to_lower()
		# Spacebar
		elif event.scancode == 32:
			current_input += " "
		# Period
		elif event.scancode == 46:
			current_input += "."
		# Backspace
		elif event.scancode == 16777220:
			if current_input.length() > 0:
				current_input = current_input.left(current_input.length() - 1)
		# Enter
		elif event.scancode == 16777221:
			process_input()
		# Escape
		elif event.scancode == 16777217:
			exit()
		update_display()

func _on_Timer_CursorBlink_timeout() -> void:
	show_cursor = !show_cursor
	update_display()

func _on_AudioStreamPlayer_finished() -> void:
	audio.stream = SOUND_RUNNING
	audio.play()

func _on_AnimationPlayer_animation_finished(anim_name : String) -> void:
	if anim_name == "shutdown":
		get_tree().paused = false
		get_tree().call_group("hud", "set_hud_visible", true)
		queue_free()

func _ready() -> void:
	get_tree().call_group("hud", "set_hud_visible", false)
	get_tree().paused = true
	audio.play()
	anim_player.play("startup")
	accept_input = false
	update_display()

