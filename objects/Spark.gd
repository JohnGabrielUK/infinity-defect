extends Node2D

onready var audio = $AudioStreamPlayer2D
onready var particles = $Particles2D

func play_sound() -> void:
	audio.play()

func _ready() -> void:
	particles.emitting = true
	yield(get_tree().create_timer(1.0), "timeout")
	queue_free()