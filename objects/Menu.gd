extends Control

const FONT_MENU = preload("res://fonts/hud.tres")

onready var audio_move = $Audio_Move
onready var audio_play = $Audio_Play
onready var audio_return = $Audio_Return
onready var audio_select = $Audio_Select

enum ITEM_TYPE { MENU, BUTTON }

onready var items = {
	"root": {
		"label": "Menu",
		"type": ITEM_TYPE.MENU,
		"items": ["play", "quit"],
		"parent": null
	},
	"play": {
		"label": "Play",
		"type": ITEM_TYPE.BUTTON
	},
	"settings": {
		"label": "Settings",
		"type": ITEM_TYPE.MENU,
		"items": ["video", "audio", "controls", "reset_save"],
		"parent": "root"
	},
	"quit": {
		"label": "Quit",
		"type": ITEM_TYPE.BUTTON
	},
	"video": {
		"label": "Video",
		"type": ITEM_TYPE.MENU,
		"items": [],
		"parent": "settings"
	},
	"audio": {
		"label": "Audio",
		"type": ITEM_TYPE.MENU,
		"items": [],
		"parent": "settings"
	},
	"controls": {
		"label": "Controls",
		"type": ITEM_TYPE.MENU,
		"items": [],
		"parent": "settings"
	},
	"reset_save": {
		"label": "Clear Save Data",
		"type": ITEM_TYPE.MENU,
		"items": ["confirm_reset_save"],
		"parent": "settings"
	},
	"confirm_reset_save": {
		"label": "Confirm?",
		"type": ITEM_TYPE.BUTTON
	}
}

export (bool) var active = false

onready var current_item_slug : String = "root"
onready var cursor_pos : int = 0

signal button_pressed

func get_item_label(slug : String) -> String:
	return items[slug]["label"]

func get_current_item() -> Dictionary:
	return items[current_item_slug]

func get_current_highlighted_item_slug() -> String:
	return get_current_item_subitems()[cursor_pos]

func get_current_highlited_item() -> Dictionary:
	return items[get_current_highlighted_item_slug()]

func get_current_item_subitems() -> Array:
	return get_current_item()["items"]

func get_current_item_slug() -> String:
	return current_item_slug

func get_current_selected_item_type() -> int:
	var selection_slug = get_current_item_subitems()[cursor_pos]
	return items[selection_slug]["type"]

func enter_menu() -> void:
	audio_select.play()
	var items = get_current_item_subitems()
	current_item_slug = items[cursor_pos]
	cursor_pos = 0

func go_back() -> void:
	audio_return.play()
	cursor_pos = 0
	var parent = get_current_item()["parent"]
	if parent != null:
		current_item_slug = parent

func cursor_up() -> void:
	audio_move.play()
	cursor_pos -= 1
	if cursor_pos < 0:
		cursor_pos = get_current_item_subitems().size() - 1

func cursor_down() -> void:
	audio_move.play()
	cursor_pos += 1
	if cursor_pos >= get_current_item_subitems().size():
		cursor_pos = 0

func select_item() -> void:
	if get_current_selected_item_type() == ITEM_TYPE.MENU:
		enter_menu()
	else:
		audio_play.play()
		emit_signal("button_pressed", get_current_highlighted_item_slug())
		print(get_current_highlighted_item_slug())

func _unhandled_input(event) -> void:
	if not active: return
	if event is InputEvent and event.pressed:
		if event.is_action_pressed("move_up"): cursor_up()
		elif event.is_action_pressed("move_down"): cursor_down()
		elif event.is_action_pressed("jump"): select_item()
		elif event.is_action_pressed("shoot"): go_back()
	update()

func _draw() -> void:
	var offset = 0
	var subitems = get_current_item_subitems()
	for i in range(0, subitems.size()):
		var current_item = subitems[i]
		var colour = Color("8b6d9c")
		if i == cursor_pos: colour = Color("fbf5ef")
		draw_string (FONT_MENU, Vector2(0, offset), get_item_label(current_item), colour )
		offset += 20
