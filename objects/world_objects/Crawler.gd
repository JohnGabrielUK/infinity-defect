extends Area2D

class_name Crawler

const OBJ_EXPLOSION = preload("res://objects/Explosion.tscn")
const OBJ_HEALTHPELLET = preload("res://objects/HealthPellet.tscn")

onready var sprite = $Sprite
onready var raycast_wall = $Sprite/RayCast2D_Wall
onready var raycast_floor = $Sprite/RayCast2D_Floor
onready var raycast_ceiling = $Sprite/RayCast2D_Ceiling
onready var audio_hit = $Audio_Hit
onready var audio_step = $Audio_Step
onready var audio_voice = $Audio_Voice
onready var audio_jump = $Audio_Jump
onready var audio_land = $Audio_Land
onready var timer = $Timer

const MOVE_SPEED : float = 32.0
const LUNGE_SPEED : float = 160.0
const JUMP_SPEED : float = 128.0
const FALL_INCR : float = 320.0
const MAX_FALL : float = 160.0
const ANIM_SPEED : float = 6.0
const SHAKE_WIDTH : float = 4.0

onready var step_sounds = [
	preload("res://sounds/crawler/step1.ogg"),
	preload("res://sounds/crawler/step2.ogg"),
	preload("res://sounds/crawler/step3.ogg"),
	preload("res://sounds/crawler/step4.ogg")
]

onready var voice_sounds = [
	preload("res://sounds/crawler/voice1.ogg"),
	preload("res://sounds/crawler/voice2.ogg"),
	preload("res://sounds/crawler/voice3.ogg"),
	preload("res://sounds/crawler/voice4.ogg"),
	preload("res://sounds/crawler/voice5.ogg")
]

enum STATE {CRAWLING, WINDUP, LUNGING, LANDED}

onready var health : int = 4
onready var current_state : int = STATE.CRAWLING
onready var direction : float = 1
onready var jump_velocity : float = 0.0
onready var shake_amount : float = 0.0
onready var step_sound_index : int = 0
onready var voice_sound_index : int = 0
onready var anim_index : float = randf() * 5.0
onready var last_step_frame : int = -1

func state_crawling(delta : float) -> void:
	anim_index += ANIM_SPEED * delta
	sprite.frame = int(anim_index) % 8
	# Move?
	if sprite.frame in [0, 2, 3, 4, 6, 7]:
		global_position.x += delta * MOVE_SPEED * direction
	# Play step sound?
	if sprite.frame in [1, 5] and last_step_frame != sprite.frame:
		play_stepsound()
	last_step_frame = int(anim_index) % 8
	# Check to turnaround
	if raycast_wall.is_colliding() or not raycast_floor.is_colliding():
		direction *= -1
		sprite.scale.x *= -1

func _on_Area2D_Eyesight_body_entered(body : PhysicsBody2D) -> void:
	if body is Player:
		if current_state == STATE.CRAWLING:
			current_state = STATE.WINDUP
			sprite.frame = 8
			timer.start()

func _on_Timer_timeout() -> void:
	if current_state == STATE.WINDUP:
		do_jump()
	elif current_state == STATE.LANDED:
		do_recover()

func state_lunging(delta : float) -> void:
	# Do a bit o' posin'
	if jump_velocity > 0.0:
		sprite.frame = 11
	else:
		sprite.frame = 9
	# Do a bit o' lungin'
	if not raycast_wall.is_colliding():
		global_position.x += delta * LUNGE_SPEED * direction
	# Do a bit o' jumpin'
	jump_velocity = clamp(jump_velocity + (FALL_INCR * delta), -JUMP_SPEED, MAX_FALL)
	if raycast_ceiling.is_colliding():
		jump_velocity = clamp(jump_velocity, 0, MAX_FALL)
	global_position.y += jump_velocity * delta
	# Maybe do a bit o' landin'?
	if raycast_floor.is_colliding() and jump_velocity > 0.0:
		do_land()

func do_jump() -> void:
	jump_velocity = -JUMP_SPEED
	current_state = STATE.LUNGING
	audio_voice.play()
	audio_jump.play()

func do_land() -> void:
	global_position.y = round(global_position.y / 16.0) * 16.0
	current_state = STATE.LANDED
	sprite.frame = 8
	timer.start()
	audio_land.play()

func do_recover() -> void:
	current_state = STATE.CRAWLING

func play_stepsound() -> void:
	audio_step.stream = step_sounds[step_sound_index]
	audio_step.play()
	step_sound_index = step_sound_index + 1
	if step_sound_index >= step_sounds.size(): step_sound_index = 0

func _physics_process(delta : float) -> void:
	# Shake it, baby
	if shake_amount > 0.0:
		sprite.offset.x = pow(shake_amount, 2.0) * SHAKE_WIDTH * ((randf() * 2.0) - 1.0)
		shake_amount -= delta / 2.0
	# Behaviour
	match current_state:
		STATE.CRAWLING: state_crawling(delta)
		STATE.LUNGING: state_lunging(delta)
		STATE.LANDED: pass

func _on_Crawler_body_entered(body : PhysicsBody2D):
	if body is Player:
		var damage = 10
		if current_state == STATE.LUNGING:
			damage = 20
		body.get_hurt(damage)

func _on_Crawler_area_entered(area : Area2D):
	if area is PlayerShot:
		area.hit()
		shake_amount = 1.00
		audio_hit.play()
		# ouchies
		health -= 1
		if health <= 0:
			die()

func die() -> void:
	var explosion = OBJ_EXPLOSION.instance()
	explosion.global_position = global_position + Vector2(0, -8)
	get_parent().add_child(explosion)
	explosion.play_bang_flesh()
	# Maybe drop health
	if randf() > 0.5:
		var health = OBJ_HEALTHPELLET.instance()
		health.global_position = global_position + Vector2(0, -8)
		get_parent().call_deferred("add_child", health)
	queue_free()


