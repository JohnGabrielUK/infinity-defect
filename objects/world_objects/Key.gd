extends Area2D

export (int, "Science", "Engine") var type

onready var audio_pickup = $Audio_Pickup
onready var collision = $CollisionShape2D

func _on_Key_body_entered(body : PhysicsBody2D) -> void:
	if body is Player:
		GameProgress.key_picked_up(type)
		hide()
		collision.set_deferred("disabled", true)
		audio_pickup.play()
		yield(audio_pickup, "finished")
		queue_free()

func _enter_tree() -> void:
	if type == 0 and GameProgress.science_clear_state == GameProgress.ROOM_STATE.CLEARED:
		queue_free()
	
