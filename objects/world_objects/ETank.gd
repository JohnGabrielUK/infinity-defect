extends Area2D

export (int) var id = -1

func _on_ETank_body_entered(body : PhysicsBody2D) -> void:
	if body is Player:
		GameProgress.etank_picked_up(id)
		body.add_health(30, false, true)
		queue_free()

func _enter_tree() -> void:
	if GameProgress.etanks_picked_up[id] == true:
		queue_free()
