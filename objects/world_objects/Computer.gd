extends Area2D

const OBJ_TERMINAL = preload("res://objects/Terminal.tscn")

onready var sprite_arrow = $Sprite_Arrow

onready var hover_index : float = 0.0

export (int) var id

var usable

func _physics_process(delta : float) -> void:
	if not usable: return
	# Floaty arrow
	hover_index += delta
	sprite_arrow.offset.y = sin(hover_index * 4.0) * 2.0
	var player_near = false
	for current_body in get_overlapping_bodies():
		if current_body is Player:
			player_near = true
			break
	sprite_arrow.visible = player_near

func interact() -> void:
	if not usable: return
	var terminal = OBJ_TERMINAL.instance()
	terminal.computer_id = id
	get_tree().root.add_child(terminal)

func _ready() -> void:
	# Make sure we're active first
	usable = true
	var gamemaster_nodes = get_tree().get_nodes_in_group("gamemaster")
	if gamemaster_nodes.size() > 0:
		var gamemaster = gamemaster_nodes[0]
		if gamemaster.self_destruct_activated:
			usable = false
