extends Node2D

onready var platform = $Platform
onready var tween = $Tween

func set_gravity_strength(strength : int) -> void: # 0 = normal, -1 = weak, 1 = strong
	var new_height = 0
	match strength:
		1: new_height = 32
		-1: new_height = -32
	tween.interpolate_property(platform, "position", platform.position, Vector2(0, new_height),
								2.0, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tween.start()
	