extends Area2D

class_name Clawguy

const OBJ_EXPLOSION = preload("res://objects/Explosion.tscn")
const OBJ_HEALTHPELLET = preload("res://objects/HealthPellet.tscn")

const ANIM_SPEED : float = 10.0
const MOVE_SPEED : float = 24.0
const FALL_INCR : float = 256.0
const MAX_FALL : float = 245.0
const SHAKE_WIDTH : float = 4.0

onready var step_sounds = [
	preload("res://sounds/clawguy/step1.ogg"),
	preload("res://sounds/clawguy/step2.ogg"),
	preload("res://sounds/clawguy/step3.ogg"),
	preload("res://sounds/clawguy/step4.ogg")
]

enum STATE {ON_CEILING, DROPPING, LANDED}

onready var sprite = $Sprite
onready var raycast_turn = $RayCast2D_Turnaround
onready var raycast_below = $RayCast2D_LookBelow
onready var raycast_ground = $RayCast2D_Ground
onready var raycast_ceiling = $RayCast2D_Ceiling
onready var timer = $Timer_Twitch
onready var audio_hit = $Audio_Hit
onready var audio_step = $Audio_Step

onready var health : int = 3
onready var direction : float = 1.0
onready var current_state : int = STATE.ON_CEILING
onready var falling_speed : float = 0.0
onready var bounce_offset : float = 0.0
onready var shake_amount : float = 0.0
onready var step_sound_index : int = 0
onready var last_step_frame : int = -1
onready var anim_index : float = randf() * 5.0

func state_onceiling(delta : float) -> void:
	global_position.x += delta * MOVE_SPEED * direction
	anim_index += ANIM_SPEED * delta
	sprite.frame = int(anim_index) % sprite.hframes
	# Play step sound?
	if sprite.frame in [2, 5] and last_step_frame != sprite.frame:
		play_stepsound()
	last_step_frame = int(anim_index) % sprite.hframes
	# Check to turnaround
	if raycast_turn.is_colliding() or not raycast_ceiling.is_colliding():
		direction *= -1
		raycast_turn.cast_to.x *= -1
		raycast_ceiling.position.x *= -1
		sprite.flip_h = !sprite.flip_h
	# Check for player
	if raycast_below.is_colliding():
		current_state = STATE.DROPPING

func state_dropping(delta : float) -> void:
	falling_speed = clamp(falling_speed + FALL_INCR * delta, 0.0, MAX_FALL)
	global_position.y += falling_speed * delta
	if raycast_ground.is_colliding():
		current_state = STATE.LANDED
		falling_speed = clamp(-falling_speed, -32.0, 0)
		timer.start(0.5 + (randf()*2.5))

func state_landed(delta : float) -> void:
	# Twitch a bit
	anim_index += ANIM_SPEED * delta * (randf() - 0.5)
	if anim_index < 0: anim_index += sprite.hframes
	sprite.frame = int(anim_index) % sprite.hframes
	# Bounce a bit
	falling_speed = clamp(falling_speed + (FALL_INCR * delta), -MAX_FALL, MAX_FALL)
	bounce_offset = clamp(bounce_offset + (falling_speed * delta), -8.0, 0.0)
	sprite.position.y = 8.0 + bounce_offset

func _physics_process(delta : float) -> void:
	# Shake it, baby
	if shake_amount > 0.0:
		sprite.offset.x = pow(shake_amount, 2.0) * SHAKE_WIDTH * ((randf() * 2.0) - 1.0)
		shake_amount -= delta / 2.0
	match current_state:
		STATE.ON_CEILING: state_onceiling(delta)
		STATE.DROPPING: state_dropping(delta)
		STATE.LANDED: state_landed(delta)

func play_stepsound() -> void:
	audio_step.stream = step_sounds[step_sound_index]
	audio_step.play()
	step_sound_index = step_sound_index + 1
	if step_sound_index >= step_sounds.size(): step_sound_index = 0

func _on_Clawguy_body_entered(body) -> void:
	if body is Player:
		body.get_hurt(10)

func _on_Timer_Twitch_timeout() -> void:
	falling_speed = -randf() * 64
	timer.start(0.5 + (randf()*2.5))

func _on_Clawguy_area_entered(area : Area2D) -> void:
	if area is PlayerShot:
		area.hit()
		shake_amount = 1.00
		audio_hit.play()
		# ouchies
		health -= 1
		if health <= 0:
			die()

func die() -> void:
	var explosion = OBJ_EXPLOSION.instance()
	explosion.global_position = global_position + Vector2(0, 8)
	get_parent().add_child(explosion)
	explosion.play_bang_flesh()
	# Maybe drop health
	if randf() > 0.5:
		var health = OBJ_HEALTHPELLET.instance()
		health.global_position = global_position + Vector2(0, 8)
		get_parent().call_deferred("add_child", health)
	queue_free()
