extends Area2D

const SHAKE_WIDTH : float = 4.0
const MAX_SPEED : float = 64.0
const ACCEL_SPEED : float = 48.0
const MAX_PLAYER_DISTANCE : float = 80.0

export (bool) var moving = true

onready var sprite = $Sprite
onready var audio_hit = $Audio_Hit

onready var anim_index : float = randf() * 5
onready var shake_amount : float = 0.0
onready var velocity : Vector2 = Vector2.ZERO

func _physics_process(delta):
	# Chase the player if they're in range
	var player_nodes = get_tree().get_nodes_in_group("player")
	if player_nodes.size() > 0 and moving:
		var player = player_nodes[0]
		if global_position.distance_to(player.global_position) < MAX_PLAYER_DISTANCE:
			velocity += global_position.direction_to(player.global_position) * ACCEL_SPEED * delta
			velocity.clamped(MAX_SPEED)
		else:
			velocity = lerp(velocity, Vector2.ZERO, delta)
	global_position += velocity * delta
	# Bob up and down a bit
	anim_index += delta * 10.0
	sprite.frame = clamp(fmod(anim_index, 10.0), 0.0, 5.0) - (clamp(fmod(anim_index, 10.0), 5.0, 10.0)-5.0)
	# Shake it, baby
	if shake_amount > 0.0:
		sprite.offset.x = pow(shake_amount, 2.0) * SHAKE_WIDTH * ((randf() * 2.0) - 1.0)
		shake_amount -= delta / 2.0

func _on_Blob_area_entered(area):
	if area is PlayerShot:
		velocity += area.velocity / 16.0
		audio_hit.pitch_scale = 0.6 + (randf() * 0.4)
		audio_hit.play()
		area.queue_free()
		shake_amount = 1.00

func _on_Blob_body_entered(body):
	if body is Player:
		body.get_hurt(10)
