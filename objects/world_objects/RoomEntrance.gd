extends Position2D

class_name RoomEntrance

export (int) var entrance_id

export (NodePath) var path_door_to_open = null

func used() -> void:
	if path_door_to_open != null:
		var door = get_node(path_door_to_open)
		door.call_deferred("open_immediately")