extends Area2D

const OBJ_EXPLOSION = preload("res://objects/Explosion.tscn")
const OBJ_HEALTHPELLET = preload("res://objects/HealthPellet.tscn")

const SHAKE_WIDTH : float = 4.0
const MOVE_SPEED : float = 80.0

onready var sprite = $Sprite
onready var area_eyesight = $Area2D_Eyesight
onready var raycast = $RayCast2D_Turnaround
onready var audio_hit = $Audio_Hit
onready var audio_alert = $Audio_Alert
onready var audio_drone = $Audio_Drone

onready var health : int = 2
onready var shake_amount : float = 0.0
onready var chasing : bool = false

onready var target_speed : float = 1.0
onready var actual_speed : float = 1.0

func _physics_process(delta):
	# Shake it, baby
	if shake_amount > 0.0:
		sprite.offset.x = pow(shake_amount, 2.0) * SHAKE_WIDTH * ((randf() * 2.0) - 1.0)
		shake_amount -= delta / 2.0
	# Move it, baby
	actual_speed = lerp(actual_speed, target_speed, delta)
	global_position.x += actual_speed * MOVE_SPEED * delta
	audio_drone.pitch_scale = 0.25 + abs(actual_speed)
	if actual_speed > 0:
		sprite.flip_h = false
		area_eyesight.scale.x = 1.0
	else:
		sprite.flip_h = true
		area_eyesight.scale.x = -1.0
	# Check that we aren't going to smack into a wall just yet
	if raycast.is_colliding() and not chasing:
		raycast.cast_to.x *= -1
		target_speed *= -1

func _on_Drone_area_entered(area):
	if area is PlayerShot:
		area.hit()
		shake_amount = 1.00
		audio_hit.play()
		# ouchies
		health -= 1
		if health <= 0:
			# Maybe drop health
			if randf() > 0.5:
				var health = OBJ_HEALTHPELLET.instance()
				health.global_position = global_position
				get_parent().call_deferred("add_child", health)
			die()

func _on_Drone_body_entered(body):
	if body is Player:
		# Deal some damage on our way out
		body.get_hurt(20)
	# Now go ahead and explode
	die()

func _on_Area2D_Eyesight_body_entered(body):
	if body is Player:
		if not chasing: audio_alert.play()
		chasing = true
		target_speed *= 4

func die():
	var explosion = OBJ_EXPLOSION.instance()
	explosion.global_position = global_position
	get_parent().add_child(explosion)
	explosion.emit_debris(3)
	explosion.play_bang_robot()
	queue_free()
