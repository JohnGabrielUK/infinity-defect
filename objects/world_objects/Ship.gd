extends Area2D

func _on_Ship_body_entered(body : PhysicsBody2D) -> void:
	if body is Player:
		var gamemaster_nodes = get_tree().get_nodes_in_group("gamemaster")
		if gamemaster_nodes.size() > 0:
			var gamemaster = gamemaster_nodes[0]
			if gamemaster.self_destruct_activated:
				gamemaster.goto_ending()
