extends KinematicBody2D

class_name Player

const OBJ_SHOT = preload("res://objects/PlayerShot.tscn")
const OBJ_EXPLODE = preload("res://objects/Explosion.tscn")

const RUN_SPEED : float = 96.0
const FLINCH_SPEED : float = 128.0
const JUMP_AMOUNT : float = 245.0
const FALLING_ACCEL : float = 480.0
const MAX_FALL : float = 245.0
const SHOT_SPEED : float = 256.0
const ANIM_SPEED : float = 6.0
const GUN_OUT_DURATION : float = 0.5

enum STATE {ON_FLOOR, CROUCHING, JUMPING, FALLING, HURT}
enum CONTROL {LEFT, RIGHT, UP, DOWN, JUMP, SHOOT}

onready var sprite = $Sprite
onready var gunpoint_standing = $Sprite/Position2D_Gunpoint_Standing
onready var gunpoint_jumping = $Sprite/Position2D_Gunpoint_Jumping
onready var gunpoint_crouching = $Sprite/Position2D_Gunpoint_Crouching
onready var collision_standing = $CollisionShape2D_Standing
onready var collision_crouching = $CollisionShape2D_Crouching
onready var camera_shaker = $CameraShaker
onready var camera = $CameraShaker/Camera
onready var audio_step = $Audio_Step
onready var audio_jump = $Audio_Jump
onready var audio_land = $Audio_Land
onready var audio_fire = $Audio_Fire
onready var audio_hurt = $Audio_Hurt
onready var audio_pickup = $Audio_Pickup
onready var audio_powerup = $Audio_Powerup
onready var audio_lowhealth = $Audio_LowHealth
onready var timer_recover = $Timer_Recover
onready var timer_blinkingtime = $Timer_BlinkingTime
onready var timer_selfdestruct_explode = $Timer_SelfDestructExplode

onready var sounds_step = [
	preload("res://sounds/player/step1.ogg"),
	preload("res://sounds/player/step2.ogg"),
	preload("res://sounds/player/step3.ogg"),
	preload("res://sounds/player/step4.ogg"),
	preload("res://sounds/player/step5.ogg")
]

var room_limits : Rect2

var special_camera_limits_set : bool = false
onready var current_state = STATE.ON_FLOOR
onready var falling_velocity : float = 0.0
var gravity_strength : float = 1.0
var anim_index : float = 0.0
var last_run_frame : int = -1
var step_sound_index : int = 0
onready var gun_out : float = 0.0

var health : int
var max_health : int
onready var vulnerable : bool = true
var self_destruct_effect : bool
onready var controls_flip_type : int = 0
onready var actions_flipped : bool = false

func skew_controls() -> void:
	controls_flip_type = randi() % 4
	if randf() > 0.5:
		actions_flipped = true
	else:
		actions_flipped = false

func get_action(control : int) -> String:
	match control:
		CONTROL.UP:
			return ["move_up", "move_right", "move_down", "move_left"][controls_flip_type]
		CONTROL.DOWN:
			return ["move_down", "move_left", "move_up", "move_right"][controls_flip_type]
		CONTROL.LEFT:
			return ["move_left", "move_up", "move_right", "move_down"][controls_flip_type]
		CONTROL.RIGHT:
			return ["move_right", "move_down", "move_left", "move_up"][controls_flip_type]
		CONTROL.JUMP:
			if actions_flipped: return "shoot"
			return "jump"
		CONTROL.SHOOT:
			if actions_flipped: return "jump"
			return "shoot"
		_:
			return ""

func side_moving(delta : float) -> bool: # Returns if we're moving or not
	var is_moving = false
	var run_velocity := Vector2.ZERO
	if Input.is_action_pressed(get_action(CONTROL.RIGHT)):
		run_velocity.x = RUN_SPEED
		sprite.scale.x = 1
		set_camera_offset(Vector2(48, 0))
		is_moving = true
	elif Input.is_action_pressed(get_action(CONTROL.LEFT)):
		run_velocity.x = -RUN_SPEED
		sprite.scale.x = -1
		set_camera_offset(Vector2(-48, 0))
		is_moving = true
	move_and_slide(run_velocity, Vector2.UP)
	return is_moving

func in_air(delta : float, fall_multiplier : float) -> void:
	move_and_slide(Vector2.DOWN * falling_velocity, Vector2.UP)
	var fall_incr : float = FALLING_ACCEL * fall_multiplier * gravity_strength * delta
	falling_velocity = clamp(falling_velocity + fall_incr, -MAX_FALL, MAX_FALL)
	if is_on_ceiling():
		falling_velocity = clamp(falling_velocity, 0.0, MAX_FALL)
	if is_on_floor():
		do_land()

func state_onfloor(delta : float) -> void:
	var is_moving = side_moving(delta)
	if is_moving:
		anim_index += delta
		sprite.frame = int(fmod(anim_index * ANIM_SPEED, 4.0))
		if gun_out > 0.0:
			sprite.frame = int(4 + fmod(anim_index * ANIM_SPEED, 4.0))
		# Footstep sound
		if sprite.frame != last_run_frame and sprite.frame in [0, 2]:
			last_run_frame = sprite.frame
			audio_step.stream = sounds_step[step_sound_index]
			audio_step.play()
			step_sound_index = step_sound_index + 1
			if step_sound_index >= 5: step_sound_index = 0
	else:
		sprite.frame = 0
		if gun_out > 0.0:
			sprite.frame = 20
	# Are we on the floor?
	if test_move(transform, Vector2.DOWN):
		if Input.is_action_just_pressed(get_action(CONTROL.JUMP)):
			do_jump()
	else:
		do_fall()
	if Input.is_action_just_pressed(get_action(CONTROL.SHOOT)):
		shoot()
	elif Input.is_action_just_pressed(get_action(CONTROL.UP)):
		interact()
	elif Input.is_action_pressed(get_action(CONTROL.DOWN)):
		do_crouch()

func state_crouching(delta : float) -> void:
	if not Input.is_action_pressed(get_action(CONTROL.DOWN)):
		do_standup()
	elif Input.is_action_just_pressed(get_action(CONTROL.SHOOT)):
		shoot()
	# Turning
	if Input.is_action_pressed(get_action(CONTROL.RIGHT)):
		sprite.scale.x = 1
		set_camera_offset(Vector2(48, 0))
	elif Input.is_action_pressed(get_action(CONTROL.LEFT)):
		sprite.scale.x = -1
		set_camera_offset(Vector2(-48, 0))
	# Sprite frame
	anim_index += delta * ANIM_SPEED
	if gun_out > 0.0:
		sprite.frame = int(22 + clamp(anim_index, 0.0, 1.0))
	else:
		sprite.frame = 21

func state_jumping(delta : float) -> void:
	side_moving(delta)
	anim_index += delta * ANIM_SPEED
	sprite.frame = int(8 + clamp(anim_index * 2.0, 0.0, 3.0))
	if gun_out > 0.0:
		sprite.frame = int(12 + clamp(anim_index, 0.0, 1.0))
	if not Input.is_action_pressed(get_action(CONTROL.JUMP)):
		in_air(delta, 3.0)
	else:
		in_air(delta, 1.0)
	if falling_velocity > 0.0:
		do_fall()
	if Input.is_action_just_pressed(get_action(CONTROL.SHOOT)):
		shoot()

func state_falling(delta : float) -> void:
	in_air(delta, 1.0)
	side_moving(delta)
	anim_index += delta * ANIM_SPEED
	sprite.frame = int(14 + clamp(anim_index / 1.5, 0.0, 1.0))
	if gun_out > 0.0:
		sprite.frame = int(12 + clamp(anim_index, 0.0, 1.0))
	if Input.is_action_just_pressed(get_action(CONTROL.SHOOT)):
		shoot()

func state_hurt(delta : float) -> void:
	sprite.frame = 24
	if sprite.scale.x > 0:
		move_and_slide(Vector2.LEFT * FLINCH_SPEED, Vector2.UP)
	else:
		move_and_slide(Vector2.RIGHT * FLINCH_SPEED, Vector2.UP)

func do_jump() -> void:
	falling_velocity = -JUMP_AMOUNT
	current_state = STATE.JUMPING
	audio_jump.play()
	anim_index = 0
	gun_out = 0.0

func do_fall() -> void:
	current_state = STATE.FALLING
	anim_index = 0.0

func do_land() -> void:
	falling_velocity = 0.0
	current_state = STATE.ON_FLOOR
	audio_land.play()
	gun_out = 0.0

func do_crouch() -> void:
	collision_standing.disabled = true
	collision_crouching.disabled = false
	current_state = STATE.CROUCHING
	sprite.frame = 21
	gun_out = 0.0

func do_standup() -> void:
	collision_standing.disabled = false
	collision_crouching.disabled = true
	current_state = STATE.ON_FLOOR
	sprite.frame = 0
	gun_out = 0.0

func shoot() -> void:
	var shot = OBJ_SHOT.instance()
	match current_state:
		STATE.ON_FLOOR:
			shot.global_position = gunpoint_standing.global_position
		STATE.CROUCHING:
			shot.global_position = gunpoint_crouching.global_position
		STATE.JUMPING:
			shot.global_position = gunpoint_jumping.global_position
		STATE.FALLING:
			shot.global_position = gunpoint_jumping.global_position
		_:
			shot.global_position = global_position
			printerr("Couldn't find proper gunpoint for player shot")
		
	shot.velocity = Vector2.RIGHT * SHOT_SPEED * sprite.scale.x
	get_parent().add_child(shot)
	audio_fire.play()
	gun_out = GUN_OUT_DURATION
	if current_state != STATE.ON_FLOOR:
		anim_index = 0

func interact() -> void:
	for current_interact in get_tree().get_nodes_in_group("interact"):
		if current_interact.overlaps_body(self):
			current_interact.interact()

func add_health(how_much : int, use_sound=false, use_other_sound=false) -> void:
	health = clamp(health + how_much, 0, max_health)
	get_tree().call_group("hud", "set_player_energy", health)
	if use_sound: audio_pickup.play()
	if use_other_sound: audio_powerup.play()
	if health > 20: audio_lowhealth.stop()

func get_hurt(damage : int) -> void:
	if not vulnerable: return
	audio_hurt.play()
	health -= damage
	if health <= 0:
		gameover()
	current_state = STATE.HURT
	vulnerable = false
	falling_velocity = 0.0
	timer_recover.start()
	get_tree().call_group("hud", "set_player_energy", health)
	if health < 20:
		audio_lowhealth.play()

func gameover() -> void:
	get_tree().call_group("gamemaster", "gameover")

func set_room_limits(limits : Rect2) -> void:
	room_limits = limits
	if not special_camera_limits_set:
		$CameraShaker/Camera.limit_left = limits.position.x
		$CameraShaker/Camera.limit_top = limits.position.y
		$CameraShaker/Camera.limit_right = limits.end.x
		$CameraShaker/Camera.limit_bottom = limits.end.y

func set_camera_limits(limits : Rect2) -> void:
	$CameraShaker/Camera.limit_left = limits.position.x
	$CameraShaker/Camera.limit_top = limits.position.y
	$CameraShaker/Camera.limit_right = limits.end.x
	$CameraShaker/Camera.limit_bottom = limits.end.y
	special_camera_limits_set = true

func reset_camera_limits(old_zone : Area2D = null) -> void:
	special_camera_limits_set = false
	set_room_limits(room_limits)
	# Check to see if we're in another zone
	for current_zone in get_tree().get_nodes_in_group("camera_zone"):
		if current_zone.overlaps_body(self) and current_zone != old_zone:
			current_zone._body_entered(self)

func set_camera_offset(to_where : Vector2) -> void:
	if camera != null:
		camera.position = to_where

func set_camera_self_destruct_effect() -> void:
	self_destruct_effect = true

func set_gravity_strength(strength : int) -> void: # 0 = normal, -1 = weak, 1 = strong
	match strength:
		0: gravity_strength = 1.0
		1: gravity_strength = 2.0
		-1: gravity_strength = 0.5

func update_game_progress() -> void:
	GameProgress.player_health = health
	GameProgress.player_max_health = max_health

func _physics_process(delta) -> void:
	if gun_out > 0.0:
		gun_out -= delta
	if self_destruct_effect:
		camera_shaker.position = Vector2(randf()-0.5, randf()-0.5) * 12.0
	# Check to see if Wing's found a way out of bounds yet
	if global_position.y > 1024.0:
		AudioMaster.play_voiceover("vo8")
	match current_state:
		STATE.ON_FLOOR: state_onfloor(delta)
		STATE.CROUCHING: state_crouching(delta)
		STATE.JUMPING: state_jumping(delta)
		STATE.FALLING: state_falling(delta)
		STATE.HURT: state_hurt(delta)

func _on_Timer_Recover_timeout() -> void:
	timer_blinkingtime.start()
	if test_move(transform, Vector2.DOWN):
		current_state = STATE.ON_FLOOR
	else:
		current_state = STATE.FALLING
		
func _on_Timer_BlinkingTime_timeout() -> void:
	vulnerable = true

func _on_Timer_SelfDestructExplode_timeout():
	var explode = OBJ_EXPLODE.instance()
	explode.global_position = camera.get_camera_screen_center() + (Vector2(randf()-0.5, randf()-0.5) * Vector2(640, 360))
	get_parent().add_child(explode)

func _unhandled_key_input(event) -> void:
	if event.is_action_pressed("ui_page_up"):
		for current_grav in get_tree().get_nodes_in_group("gravity"):
			current_grav.set_gravity_strength(0)
	if event.is_action_pressed("ui_page_down"):
		for current_grav in get_tree().get_nodes_in_group("gravity"):
			current_grav.set_gravity_strength(1)

func _ready() -> void:
	health = GameProgress.player_health
	max_health = GameProgress.player_max_health
	if health < 20:
		audio_lowhealth.play()
	get_tree().call_group("hud", "set_player_energy", health)
	var gamemaster_nodes = get_tree().get_nodes_in_group("gamemaster")
	if gamemaster_nodes.size() > 0:
		var gamemaster = gamemaster_nodes[0]
		self_destruct_effect = gamemaster.self_destruct_activated
		if self_destruct_effect: timer_selfdestruct_explode.start()
