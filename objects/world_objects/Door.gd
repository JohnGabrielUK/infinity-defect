extends StaticBody2D

const ANIM_SPEED : float = 15.0

onready var sprite = $Sprite
onready var collision = $CollisionShape2D
onready var timer = $Timer
onready var audio_open = $Audio_Open
onready var audio_close = $Audio_Close

var frame_target : float = 0.0
var frame_actual : float = 0.0
var open : bool = false

func open_immediately() -> void:
	frame_target = sprite.hframes - 1
	frame_actual = frame_target
	sprite.frame = frame_actual
	open = true
	layers = 0
	timer.start()

func _physics_process(delta : float) -> void:
	if frame_actual < frame_target:
		frame_actual += delta * ANIM_SPEED
	elif frame_actual > frame_target:
		frame_actual -= delta * ANIM_SPEED
	sprite.frame = int(round(frame_actual))

func _area_entered(area):
	if open: return
	if area is PlayerShot:
		area.queue_free()
		layers = 0
		frame_target = sprite.hframes - 1
		open = true
		timer.start()
		audio_open.play()

func _on_Timer_timeout():
	layers = 1
	frame_target = 0
	open = false
	audio_close.play()
