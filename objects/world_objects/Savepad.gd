extends Area2D

onready var sprite_arrow = $Sprite_Arrow
onready var audio_save = $Audio_Save

onready var hover_index : float = 0.0

export (int) var id

var usable

func _physics_process(delta : float) -> void:
	if not usable: return
	# Floaty arrow
	hover_index += delta
	sprite_arrow.offset.y = sin(hover_index * 4.0) * 2.0
	var player_near = false
	for current_body in get_overlapping_bodies():
		if current_body is Player:
			player_near = true
			break
	sprite_arrow.visible = player_near

func interact() -> void:
	if not usable: return
	audio_save.play()
	GameProgress.current_entrance = 99
	get_tree().call_group("player", "add_health", 1000)
	GameProgress.save_game()
	get_tree().call_group("hud", "show_prompt", "Game saved", 0.1)

func _ready() -> void:
	# Make sure we're active first
	usable = true
	var gamemaster_nodes = get_tree().get_nodes_in_group("gamemaster")
	if gamemaster_nodes.size() > 0:
		var gamemaster = gamemaster_nodes[0]
		if gamemaster.self_destruct_activated:
			usable = false
			sprite_arrow.hide()
