extends Area2D

const OBJ_ENERGY_ORB = preload("res://objects/EnergyOrb.tscn")
const OBJ_EXPLODE = preload("res://objects/Explosion.tscn")

const LOOP_INCR : float = 0.75
const LOOP_SIZE : Vector2 = Vector2(160, 64)
const ANIM_SPEED : float = 12.0
const CONTAINER_MAX_HEALTH : int = 50
const ACTUAL_MAX_HEALTH : int = 40
const ORB_POSITIONS : Array = [
	Vector2(-64, -32),
	Vector2(-80, 0),
	Vector2(-64, 32),
	Vector2(64, -32),
	Vector2(80, 0),
	Vector2(64, 32)
]
const SHAKE_WIDTH : float = 1.0

enum STATE {WAITING, WAKING_UP, DETACHED, ATTACHED, DYING, DEAD}

export (NodePath) var path_laser
onready var laser = get_node(path_laser)

onready var sprite_attached = $Sprite_Attached
onready var sprite_deatached = $Sprite_Detached
onready var sprite_explode = $Sprite_Explode
onready var audio_wakeup = $Audio_Wakeup
onready var audio_hit = $Audio_Hit
onready var audio_detach = $Audio_Detach
onready var audio_reattach = $Audio_Reattach
onready var audio_detached_drone = $Audio_Detached_Drone
onready var audio_dying = $Audio_Dying
onready var audio_explode = $Audio_Explode
onready var audio_music = $Audio_Music
onready var timer = $Timer_ChooseNextMove
onready var tween = $Tween
onready var game_freeze = $GameFreeze

onready var current_state : int = STATE.WAITING
onready var original_position : Vector2 = global_position
onready var loop_index : float = 0.0
onready var anim_index : float = 0.0
onready var shake_amount : float = 0.0
onready var used_freeze_trick : bool = false

onready var container_health : int = CONTAINER_MAX_HEALTH
onready var actual_health : int = ACTUAL_MAX_HEALTH

func detach() -> void:
	audio_detach.play()
	laser.stop_laser()
	current_state = STATE.DETACHED
	global_position = original_position
	loop_index = 1.575
	sprite_deatached.show()
	sprite_attached.hide()
	audio_detached_drone.play()

func reattach() -> void:
	audio_reattach.play()
	container_health = CONTAINER_MAX_HEALTH
	current_state = STATE.ATTACHED
	global_position = original_position
	sprite_deatached.hide()
	sprite_attached.show()
	audio_detached_drone.stop()
	timer.start()

func fire_orbs() -> void:
	for current_position in ORB_POSITIONS:
		var orb = OBJ_ENERGY_ORB.instance()
		orb.global_position = global_position
		orb.standby_position = global_position + current_position
		get_parent().add_child(orb)
		yield(get_tree().create_timer(0.5), "timeout")
	timer.start(8.0)

func choose_next_move() -> void:
	var choice = randf()
	if choice > 0.5:
		laser.start_laser()
		yield(laser, "laser_stopped")
		timer.start(2.0)
	else:
		fire_orbs()

func _physics_process(delta : float) -> void:
	var player = null
	var player_nodes = get_tree().get_nodes_in_group("player")
	if player_nodes.size() > 0:
		player = player_nodes[0]
	# Look at the player
	var dir_to_player = global_position.direction_to(player.global_position)
	sprite_attached.offset = dir_to_player * 2.0
	# Shake it, baby
	if shake_amount > 0.0:
		sprite_attached.offset.x += pow(shake_amount, 2.0) * SHAKE_WIDTH * ((randf() * 2.0) - 1.0)
		shake_amount -= delta / 2.0
	match current_state:
		STATE.WAITING:
			if global_position.distance_to(player.global_position) < 128 and not GameProgress.final_boss_destroyed:
				audio_wakeup.play()
				sprite_attached.show()
				tween.interpolate_property(sprite_attached, "frame", 0, 4, 0.5, Tween.TRANS_LINEAR, Tween.EASE_IN)
				tween.start()
				current_state = STATE.WAKING_UP
				yield(get_tree().create_timer(1.5), "timeout")
				audio_music.play()
				current_state = STATE.ATTACHED
				timer.start()
		STATE.DETACHED:
			# Sprite animation
			anim_index += ANIM_SPEED * delta
			sprite_deatached.frame = int(anim_index) % sprite_deatached.hframes
			# Move around a bit
			loop_index += LOOP_INCR * delta
			global_position = original_position + (Vector2(-cos(loop_index), sin(loop_index * 2.0)) * LOOP_SIZE)
			audio_detached_drone.pitch_scale = 1.0 + abs(sin(loop_index * 2.0))
			# Shake it, baby
			if shake_amount > 0.0:
				sprite_deatached.offset.x = pow(shake_amount, 2.0) * SHAKE_WIDTH * ((randf() * 2.0) - 1.0)
				shake_amount -= delta / 2.0
			if loop_index > 14.125:
				reattach()
		STATE.DYING:
			loop_index += LOOP_INCR * 0.25 * delta
			global_position = original_position + (Vector2(-cos(loop_index), sin(loop_index * 2.0)) * LOOP_SIZE)
			if audio_detached_drone.pitch_scale > 0.1:
				audio_detached_drone.pitch_scale -= delta / 4.0

func _on_Timer_ChooseNextMove_timeout() -> void:
	if current_state != STATE.ATTACHED: return
	choose_next_move()

func _on_FinalBoss_area_entered(area):
	if area is PlayerShot:
		area.hit()
		shake_amount = 1.00
		audio_hit.play()
		# Distribute damage depending on which state we're in
		if current_state == STATE.ATTACHED:
			container_health -= 1
			if container_health <= 0:
				detach()
		elif current_state == STATE.DETACHED:
			actual_health -= 1
			if actual_health <= 0:
				die()
			elif actual_health < 20 and not used_freeze_trick:
				game_freeze.do_freeze()
				used_freeze_trick = true

func die():
	audio_dying.play()
	current_state = STATE.DYING
	tween.interpolate_property(audio_music, "volume_db", audio_music.volume_db, -40, 2.0, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()
	for i in range(0, 50):
		var explode = OBJ_EXPLODE.instance()
		explode.global_position = global_position + (Vector2(randf()-0.5, randf()-0.5) * 64.0)
		get_parent().add_child(explode)
		yield(get_tree().create_timer(0.1), "timeout")
	audio_explode.play()
	audio_detached_drone.stop()
	audio_music.stop()
	get_tree().call_group("gamemaster", "begin_self_destruct")
	sprite_deatached.hide()
	sprite_explode.show()
	yield(get_tree().create_timer(1.0), "timeout")
	tween.interpolate_property(sprite_explode, "modulate", Color.white, Color(1.0, 1.0, 1.0, 0.0), 4.0, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	tween.start()
	yield(tween, "tween_all_completed")
	hide()
	current_state = STATE.DEAD

func _on_FinalBoss_body_entered(body):
	if not current_state in [STATE.ATTACHED, STATE.DETACHED]: return
	if body is Player:
		body.get_hurt(20)
