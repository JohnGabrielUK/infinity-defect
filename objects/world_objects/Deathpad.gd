extends Area2D

onready var sprite_skull = $Sprite_Skull
onready var audio_kill = $Audio_Kill

onready var hover_index : float = 0.0
onready var powered : bool = false

signal powered_up

func _physics_process(delta : float) -> void:
	if not powered:
		hover_index += delta
		sprite_skull.offset.y = sin(hover_index * 2.0) * 2.0
		sprite_skull.visible = !sprite_skull.visible

func _on_Deathpad_area_entered(area : Area2D) -> void:
	if not powered:
		if area is Crawler or area is Clawguy:
			audio_kill.play()
			area.die()
			powered = true
			emit_signal("powered_up")
			sprite_skull.hide()

func _on_Deathpad_body_entered(body : PhysicsBody2D) -> void:
	if not powered:
		if body is Player:
			body.get_hurt(30)

