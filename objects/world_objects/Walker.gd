extends Area2D

const OBJ_MISSILE = preload("res://objects/Missile.tscn")
const OBJ_EXPLOSION = preload("res://objects/Explosion.tscn")
const OBJ_HEALTHPELLET = preload("res://objects/HealthPellet.tscn")
const OBJ_KEY = preload("res://objects/world_objects/Key.tscn")

onready var anim_player = $AnimationPlayer
onready var raycast_left = $RayCast2D_WallLeft
onready var raycast_right = $RayCast2D_WallRight
onready var audio_hit = $Audio_Hit
onready var firepoint = $Sprite_Body/Position2D_Firepoint

onready var health : int = 30
onready var direction : int = 1
onready var active : bool = false

export (float) var velocity = 0

func decide() -> void:
	if direction == 1:
		if not raycast_right.is_colliding():
			anim_player.play("step_right")
		else:
			if not raycast_left.is_colliding():
				direction *= -1
				anim_player.play("step_left")
			else:
				anim_player.play("idle")
	else:
		if not raycast_left.is_colliding():
			anim_player.play("step_left")
		else:
			if not raycast_right.is_colliding():
				direction *= -1
				anim_player.play("step_right")
			else:
				anim_player.play("idle")

func _process(delta : float) -> void:
	global_position.x += velocity * delta

func _animation_finished(anim_name : String) -> void:
	decide()

func _ready() -> void:
	decide()

func _on_Walker_area_entered(area : Area2D) -> void:
	if area is PlayerShot:
		area.hit()
		#shake_amount = 1.00
		audio_hit.play()
		# ouchies
		health -= 1
		if health <= 0:
			die()

func die() -> void:
	for i in range(0, 5):
		var explosion = OBJ_EXPLOSION.instance()
		explosion.global_position = global_position + (Vector2(randf()-0.5, randf()-1.0) * 32)
		get_parent().add_child(explosion)
		explosion.emit_debris(3)
		var health = OBJ_HEALTHPELLET.instance()
		health.global_position = global_position + (Vector2(randf()-0.5, randf()-1.0) * 32)
		get_parent().call_deferred("add_child", health)
	# Spawn a key
	var key = OBJ_KEY.instance()
	key.global_position = global_position + Vector2(0, -16)
	key.type = 1
	get_parent().call_deferred("add_child", key)
	queue_free()

func _on_Walker_body_entered(body) -> void:
	if body is Player:
		body.get_hurt(10)

func _enter_tree() -> void:
	if GameProgress.engine_clear_state == GameProgress.ROOM_STATE.CLEARED:
		queue_free()

func _on_Timer_FireMissiles_timeout():
	pass
	#var missile = OBJ_MISSILE.instance()
	#missile.global_position = firepoint.global_position
	#get_parent().add_child(missile)
	
