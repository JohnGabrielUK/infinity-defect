extends Area2D

export (String) var destination_room
export (int) var destination_entrance_id

signal room_exited

func _body_entered(body):
	if body is Player:
		emit_signal("room_exited", destination_room, destination_entrance_id)
		