extends StaticBody2D

onready var sprite : Sprite = $Sprite
onready var audio_open : AudioStreamPlayer2D = $Audio_Open
onready var collision : CollisionShape2D = $CollisionShape2D
onready var tween : Tween = $Tween

func open() -> void:
	audio_open.play()
	collision.set_deferred("disabled", true)
	tween.interpolate_property(sprite, "frame", 0, 5, 0.5, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()
