extends Label

onready var time_remaining : float = 100.0

func _physics_process(delta : float) -> void:
	time_remaining = clamp(time_remaining - delta, 0.0, 1000.0)
	var readout = "T-%d:%d.%d"
	var msec = fmod(time_remaining, 1.0) * 1000
	var sec = fmod(time_remaining, 60.0)
	var minute = floor(time_remaining / 60.0)
	text = "%02d:%02d.%03d" % [minute, sec, msec]
	# Bang time?
	if time_remaining == 0.0:
		print("bang!")
