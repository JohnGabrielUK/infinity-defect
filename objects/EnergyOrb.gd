extends Area2D

const ATTACK_SPEED = 192.0
const ANIM_SPEED = 15.0

onready var sprite = $Sprite
onready var audio_launch = $Audio_Launch
onready var audio_spawn = $Audio_Spawn

onready var launched : bool = false
onready var direction : Vector2 = Vector2.DOWN
onready var anim_index : float = 0.0

var standby_position : Vector2

func _physics_process(delta : float) -> void:
	anim_index += delta
	sprite.frame = int(anim_index * ANIM_SPEED) % sprite.hframes
	if not launched:
		global_position = lerp(global_position, standby_position, delta * 2.0)
	else:
		global_position += direction * ATTACK_SPEED * delta

func _on_Timer_Launch_timeout() -> void:
	# Try to find the player
	var player_nodes = get_tree().get_nodes_in_group("player")
	if player_nodes.size() > 0:
		var player = player_nodes[0]
		direction = global_position.direction_to(player.global_position)
	launched = true
	audio_launch.play()

func _on_EnergyOrb_body_entered(body : PhysicsBody2D) -> void:
	if body is Player:
		body.get_hurt(20)

func _ready() -> void:
	audio_spawn.play()
