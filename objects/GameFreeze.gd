extends CanvasLayer

onready var whiteout = $Whiteout
onready var tween = $Tween

var filter = null

func do_freeze() -> void:
	if filter != null:
		filter.get_material().set_shader_param("time_multiplier", 0.0)
	AudioServer.set_bus_mute(0, true)
	get_tree().paused = true
	yield(get_tree().create_timer(3.0), "timeout")
	tween.interpolate_property(whiteout, "color", Color(1.0, 1.0, 1.0, 0.0), Color(1.0, 1.0, 1.0, 0.25), 1.0, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()
	OS.set_window_title("Infinity Defect (Not Responding)")
	yield(get_tree().create_timer(3.0), "timeout")
	AudioServer.set_bus_mute(0, false)
	yield(get_tree().create_timer(2.0), "timeout")
	get_tree().paused = false
	if filter != null:
		filter.get_material().set_shader_param("time_multiplier", 15.0)
	yield(get_tree().create_timer(1.0), "timeout")
	OS.set_window_title("Infinity Defect")
	tween.interpolate_property(whiteout, "color", Color(1.0, 1.0, 1.0, 0.25), Color(1.0, 1.0, 1.0, 0.0), 2.5, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()

func _ready() -> void:
	var filter_nodes = get_tree().get_nodes_in_group("filter")
	if filter_nodes.size() > 0:
		filter = filter_nodes[0]
	else:
		printerr("Couldn't get filter node.")
