extends Area2D

class_name PlayerShot

const OBJ_SPARK = preload("res://objects/Spark.tscn")

var velocity : Vector2 = Vector2.ZERO

func _physics_process(delta : float) -> void:
	global_position += velocity * delta

func _on_Timer_timeout() -> void:
	queue_free()

func _body_entered(body) -> void:
	# For now, if we hit a body, let's just assume we should stop.
	# If only all bullets were so thoughtful.
	hit(body is TileMap)

func hit(play_sound : bool = false) -> void:
	var spark = OBJ_SPARK.instance()
	spark.global_position = global_position
	get_parent().add_child(spark)
	if play_sound: spark.play_sound()
	queue_free()
