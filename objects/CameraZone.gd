extends Area2D

onready var corner = $Corner

func _body_entered(body) -> void:
	if body is Player:
		body.set_camera_limits(Rect2(global_position, corner.global_position - global_position))

func _body_exited(body) -> void:
	if body is Player:
		body.reset_camera_limits(self)
