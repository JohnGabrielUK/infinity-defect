extends Sprite

const OBJ_DEBRIS = preload("res://objects/Debris.tscn")

const ANIM_SPEED : float = 20.0
const DEBRIS_FORCE : float = 200.0

onready var audio_bang_robot = $Audio_RobotBang
onready var audio_bang_flesh = $Audio_FleshBang

onready var anim_index : float = 0.0
var anim_speed_multiplier : float = 1.0

func play_bang_robot() -> void:
	audio_bang_robot.play()

func play_bang_flesh() -> void:
	audio_bang_flesh.play()

func emit_debris(how_much : int) -> void:
	for i in range(0, how_much):
		var debris : RigidBody2D = OBJ_DEBRIS.instance()
		debris.global_position = global_position
		# This makes quite a funny glitch - save it for later, maybe?
		#debris.applied_force = Vector2(randf()-0.5, randf()-0.5) * DEBRIS_FORCE
		get_parent().call_deferred("add_child", debris)
		debris.apply_impulse(Vector2.ZERO, Vector2(randf()-0.5, randf()-0.5).normalized() * DEBRIS_FORCE)

func _physics_process(delta : float) -> void:
	anim_index += delta * anim_speed_multiplier * ANIM_SPEED
	if anim_index >= hframes:
		hide()
		var audio_playing = false
		for current_audio in [audio_bang_robot, audio_bang_flesh]:
			if current_audio.playing: audio_playing = true
		if not audio_playing:
			queue_free()
	else:
		frame = int(anim_index)

func _on_Audio_finished():
	queue_free()
