extends RigidBody2D

onready var sprite = $Sprite
onready var tween = $Tween

func _ready():
	sprite.frame = rand_range(0, sprite.hframes)
	yield(get_tree().create_timer(2.0), "timeout")
	tween.interpolate_property(sprite, "modulate", Color.white, Color(1.0, 1.0, 1.0, 0.0), 1.0, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.start()
	yield(tween, "tween_all_completed")
	queue_free()
