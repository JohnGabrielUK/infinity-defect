extends Node2D

const DAMAGE : int = 30
const ROTATE_SPEED : float = 22.5

onready var sprite_center = $Sprite_Center
onready var sprite_horizontal = $Sprite_Horizontal
onready var sprite_vertical = $Sprite_Vertical
onready var audio_bweeeeeem = $Audio_Bweeeeeem
onready var timer = $Timer
onready var tween = $Tween
onready var raycasts = [$RayCast2D_East, $RayCast2D_North, $RayCast2D_South, $RayCast2D_West]

onready var beam_active : bool = false

onready var rotate_direction : int = 1

signal laser_stopped

func start_laser() -> void:
	audio_bweeeeeem.play()
	rotation_degrees = 45
	show()
	tween.interpolate_property(sprite_center, "scale", Vector2(0, 0), Vector2(1, 1), 0.25, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.interpolate_property(sprite_horizontal, "scale", Vector2(0, 1024), Vector2(1, 1024), 0.25, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.interpolate_property(sprite_vertical, "scale", Vector2(0, 1024), Vector2(1, 1024), 0.25, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.start()
	yield(tween, "tween_all_completed")
	beam_active = true
	timer.start()

func stop_laser() -> void:
	if not beam_active: return
	audio_bweeeeeem.stop()
	beam_active = false
	tween.interpolate_property(sprite_center, "scale", Vector2(1, 1), Vector2(0, 0), 0.25, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.interpolate_property(sprite_horizontal, "scale", Vector2(1, 1024), Vector2(0, 1024), 0.25, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.interpolate_property(sprite_vertical, "scale", Vector2(1, 1024), Vector2(0, 1024), 0.25, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.start()
	yield(tween, "tween_all_completed")
	hide()
	emit_signal("laser_stopped")

func _physics_process(delta : float) -> void:
	rotation_degrees += ROTATE_SPEED * delta * rotate_direction
	if beam_active:
		for current_raycast in raycasts:
			if current_raycast.is_colliding():
				var collider = current_raycast.get_collider()
				if collider is Player:
					collider.get_hurt(DAMAGE)

func _on_Timer_timeout():
	if beam_active:
		stop_laser()

func _on_Timer_ChangeDirection_timeout():
	rotate_direction *= -1

