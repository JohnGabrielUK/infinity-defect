extends Area2D

const MAX_SPEED : float = 16.0
const CHANGE_SPEED : float = 180.0

onready var sprite = $Sprite

var player

onready var velocity : Vector2 = Vector2.UP * MAX_SPEED

func _physics_process(delta : float) -> void:
	if player != null:
		var target_velocity = global_position.direction_to(player.global_position) * MAX_SPEED
		velocity = lerp(velocity, target_velocity, delta / 2.0)
	global_position += velocity * delta

func _ready() -> void:
	var player_nodes = get_tree().get_nodes_in_group("player")
	if player_nodes.size() > 0:
		player = player_nodes[0]
