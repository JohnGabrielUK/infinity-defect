
# Infinity Defect Design Document

## Areas

### Hubs & corridors
These areas make an effort to look civilian, in the same way a cheap passenger jet does; it's not military, but it's not exactly comfortable. The walls are a sea of plastic panels and compartments, with notices and indicators everywhere.
#### Objects
**Air-lock**: In their infinite wisdom, management decided that splurging on devices that kept their expensive scientists from being asphyxiated was probably within the budget. These can either be the Metroid-style bubble hatches, or the sort of vertically-sliding doors you'd see on the Death Star.
**Save-point**: A machine that restores the player's health and saves the game.
#### Enemies
**Glitched scientists:** Something between a ghost and a zombie; these people were once civilian occupants of this station, but have been warped and broken by the anomaly. Most of the time, they move normally, only occasionally snapping between poses. Chunks of their bodies and clothes are neatly missing - almost like the game isn't rendering them properly. They don't have any attacks, but they cause damage on physical contact.

### Hangar & engine rooms
Pipes, hydralic cylinders, air vents and greebles abound. The floors are metal grates that clank noisily underfoot, and the lighting is harsh and uneven. No civilian was meant to see this part of the ship, and it shows; it hasn't had a lick of paint in years.
#### Objects
**Your ship**: A pimped-up scout fighter. Seats one. Hasn't passed its MOD in years, due to a broken indicator you could never be bothered to fix.
**Floating platforms**: Why bother designing a station to have everything properly accessible when you can just leave a few of these things lying around? Using a tiny little thruster, they're just about able to lift their own weight, and maybe also that of a particularly under-fed cat. Gravitational anomalies will cause them to either raise or lower.
#### Enemies
**Hazardous substances**: What would you get if you had the contents of a massive lavalamp floating in midair, under fluctuating amounts of gravity? Answer: a mess, a letter from the office of health and safety, and a pressing need to step carefully.

### Science labs
Visually similar to the hubs & corridors (i.e. they use the same tileset), but with huge machines and computers lining the walls. What do they do? No idea! Science, probably.
#### Objects
**Glitch core**: A floating glitch, ready to be picked up and harnessed.

### Restricted area
This place used to look like another civilian zone, but the anomaly has twisted it beyond recognition. The walls are lit with an eerie, blood-red glow, and shadows spring seemingly from nowhere. Chunks of space have even started to disappear, replaced with a void that hurts the eye to stare at.

#### Enemies
**Glitch fragments**: chunks of garbage data that float around space, harming the player if they come into contact.
**The AI Core**: A giant robotic eye (maybe reminiscent of HAL 9000's eyepiece?). It seems to be under the impression that this is a bullet-hell shooter.

## Rooms

| Name | Hazards | Exits |
| --- | --- | --- |
| Hangar | No hazards| Corridor |
| Corridor A | Some enemies, so the players gets used to evasion/combat | Hangar, Hub A |
| Hub A | No hazards | Corridor A, Save Room A, Engine corridor
| Save room A | No hazards  | Hub A, Science Lab 1 |
| Engine corridor | Some enemies; just to get used to high/low grav | Hub A, Engine room
| Engine room | Lots of enemies; need to deal with them and grav conditions | Engine corridor
| Science lab 1 | No enemies; safe area for getting used to the blocks | Save room A, Science lab 2
| Science lab 2 | More challenging blocks, with hazards below if you fall | Science lab 1
| Obstacle course | Enemies. You need both glitches to get past | Hub A, Maze |
| Maze | Enemies; the room's exits make it loop around on itself | Obstacle course, Hub B
| Hub B | No enemies | Maze, Save room B, Cockpit, Restricted area 1 |
| Save room B | No hazards | Hub B |
| Cockpit | No hazards - logs reveal backstory | Hub B |
| Restricted area 1 | Lots of enemies. Player's jump & shoot controls are swapped | Hub B, Restricted area 2 |
| Restricted area 2 | Lots of enemies. Player's directional controls keep rotating | Restricted area 1, Boss room |
| Boss room | I mean, the boss is pretty hazardous. | Restricted area 2 |
