extends CanvasLayer

onready var filter = $Filter
onready var anim_player = $AnimationPlayer

func set_shader_parameters(enabled : bool, tear_limit : float, loops : int, start_power : int,
							time_multiplier : float, step_amount : float, power : float, resolution: float):
	filter.get_material().set_shader_param("glitches_enabled", enabled)
	filter.get_material().set_shader_param("tear_limit", tear_limit)
	filter.get_material().set_shader_param("loops", loops)
	filter.get_material().set_shader_param("start_power", start_power)
	filter.get_material().set_shader_param("time_multiplier", time_multiplier)
	filter.get_material().set_shader_param("step_amount", step_amount)
	filter.get_material().set_shader_param("power", power)
	filter.get_material().set_shader_param("resolution", resolution)

func _ready():
	get_tree().paused = false
	anim_player.play("gameover")
	var glitchiness = 2.0
	while glitchiness > 0.0:
		glitchiness -= 0.05
		set_shader_parameters(true, 1, 5, 0, 30, glitchiness, 1.5, 8)
		yield(get_tree().create_timer(0.1), "timeout")
	yield(get_tree().create_timer(1.0), "timeout")

func _on_AnimationPlayer_animation_finished(anim_name):
	GameProgress.restore_saved_game()
	get_tree().change_scene("res://scenes/Game.tscn")
