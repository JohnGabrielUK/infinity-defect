extends Control

onready var filter = $Filter
onready var anim_player = $AnimationPlayer
onready var audio = $AudioStreamPlayer
onready var video = $VideoPlayer

export (bool) var intro_done = false

func set_shader_parameters(enabled : bool, tear_limit : float, loops : int, start_power : int,
							time_multiplier : float, step_amount : float, power : float, resolution: float):
	filter.get_material().set_shader_param("glitches_enabled", enabled)
	filter.get_material().set_shader_param("tear_limit", tear_limit)
	filter.get_material().set_shader_param("loops", loops)
	filter.get_material().set_shader_param("start_power", start_power)
	filter.get_material().set_shader_param("time_multiplier", time_multiplier)
	filter.get_material().set_shader_param("step_amount", step_amount)
	filter.get_material().set_shader_param("power", power)
	filter.get_material().set_shader_param("resolution", resolution)

func _ready():
	set_shader_parameters(true, 1.0, 10, -2, 45, 0.2, 1.7, 3)
	yield(get_tree().create_timer(2.0), "timeout")
	anim_player.play("intro")
	audio.play()
	video.play()

func _unhandled_input(event):
	if event is InputEventKey:
		if event.pressed:
			if not intro_done:
				anim_player.seek(9.4)

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "begin":
		get_tree().change_scene("res://scenes/Intro.tscn")

func _on_VideoPlayer_finished():
	video.play()

func _on_Menu_button_pressed(slug : String) -> void:
	match slug:
		"play":
			GameProgress.new_game()
			anim_player.play("begin")
		"quit": get_tree().quit()
