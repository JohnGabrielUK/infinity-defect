extends Node2D

const STORY : Array = [
	"The \"quantum cascade\" was known by many names, but none were more apt than \"anomaly\". It defied any explanation; its nature would change the instant any satisfactory description was reached.\n\nIt was a fault in the laws of physics. A fumble of the dicemaster. A headache for anyone giving a lecture on astrophysics that week.",
	"Its area of influence had grown in the six months since it had appeared. Observation station C4-R7 had been assigned to monitor it, and got swept straight into the event horizon.\n\nThey lost contact after twelve days.",
	"Several generals had to swallow their pride before they asked me to investigate, but given my track record in dealing with the inexplicable, they had no choice. No-one wanted to go near the thing. Even I didn't.\n\nMy mission was clear. Contain, or destroy. The crew's family would thank me if I could bring them home. Everyone else would rather I couldn't."
]

onready var label_story = $Label_Story
onready var audio_music = $Audio_Music
onready var anim_player = $AnimationPlayer
onready var sprite_arrow = $Sprite_Arrow
onready var blackout = $CanvasLayer/Blackout
onready var filter = $CanvasLayer/Filter
onready var timer_tick = $Timer_Tick
onready var tween = $Tween

onready var hover_index : float = 0.0
onready var current_paragraph : int = 0
onready var showing_story : bool = false

func _physics_process(delta : float) -> void:
	hover_index += delta
	sprite_arrow.offset.y = sin(hover_index * 4.0) * 2.0

func _input(event) -> void:
	if not showing_story: return
	if event.is_action_pressed("jump") and label_story.percent_visible >= 1.00:
		sprite_arrow.hide()
		if current_paragraph < 2:
			current_paragraph += 1
			show_next_paragraph()
		else:
			play_intro()

func _on_Timer_Tick_timeout() -> void:
	label_story.visible_characters += 1
	if label_story.percent_visible >= 1.00:
		sprite_arrow.show()
		timer_tick.stop()

func show_next_paragraph() -> void:
	showing_story = true
	tween.interpolate_property(label_story, "modulate", Color.white, Color(1.0, 1.0, 1.0, 0.0), 0.5, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()
	yield(tween, "tween_all_completed")
	label_story.text = STORY[current_paragraph]
	label_story.visible_characters = 0
	label_story.modulate = Color.white
	timer_tick.start()

func play_intro() -> void:
	showing_story = false
	tween.interpolate_property(label_story, "modulate", Color.white, Color(1.0, 1.0, 1.0, 0.0), 3.0, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()
	yield(tween, "tween_all_completed")
	anim_player.play("intro")

func _on_AnimationPlayer_animation_finished(anim_name):
	tween.interpolate_property(blackout, "modulate", Color(1.0, 1.0, 1.0, 0.0), Color.white, 1.0, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()
	yield(tween, "tween_all_completed")
	get_tree().change_scene("res://scenes/Game.tscn")

func _ready() -> void:
	filter.get_material().set_shader_param("glitches_enabled", false)
	tween.interpolate_property(blackout, "modulate", Color.white, Color(1.0, 1.0, 1.0, 0.0), 3.0, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()
	audio_music.play()
	yield(get_tree().create_timer(2.0), "timeout")
	show_next_paragraph()

