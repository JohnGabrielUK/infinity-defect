extends Control

onready var anim_player = $AnimationPlayer

func _ready() -> void:
	anim_player.play("credits")

func _on_AnimationPlayer_animation_finished(anim_name):
	get_tree().change_scene("res://scenes/TitleScreen.tscn")
