extends Node2D

const OBJ_EXPLOSION = preload("res://objects/Explosion.tscn")

onready var sprite_station : Sprite = $Sprite_Station
onready var anim_player : AnimationPlayer = $AnimationPlayer
onready var timer_explosion : Timer = $Timer_Explosion
onready var filter = $CanvasLayer/Filter

export (bool) var emit_explosions = true

func _on_Timer_Explosion_timeout():
	if emit_explosions:
		var explode = OBJ_EXPLOSION.instance()
		explode.anim_speed_multiplier = 0.5
		sprite_station.add_child(explode)
		explode.position = Vector2(randf()-0.5, randf()-0.5) * Vector2(150, 90)

func _ready() -> void:
	filter.get_material().set_shader_param("glitches_enabled", false)
	yield(get_tree().create_timer(1.0), "timeout")
	anim_player.play("ending")
	timer_explosion.start()

func _on_AnimationPlayer_animation_finished(anim_name):
	get_tree().change_scene("res://scenes/Credits.tscn")
